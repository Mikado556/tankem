import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\..\\..\\common\\internal"
sys.path.append(cheminFichierMain)
from DTO import *
from Tkinter import *

class Vue():
    def __init__(self):
        self.dtoMap = dtoOracleMap.DtoMap().getMapExistant()
        self.root = Tk()
        self.root.title("Menu Niveau")
        self.frameMenuSelect = Frame(self.root,width = 450, height = 300)
        self.frameMenuSelect.pack_propagate(False)
        self.listbox = Listbox(self.frameMenuSelect,width=50, height=10)
        self.labelCategoie = Label(self.frameMenuSelect,text = "idNiveau, Titre, Date, delaiMinimun, delaiMaximun, statut, idUtilisateur, idMap")
        #self.listbox.insert(END, self.dtoMap[1])
        for x in self.dtoMap:
            self.listbox.insert(END, x)
        self.listbox.bind("<Key>", self.selection)
        self.listbox.select_set(0)
        self.listbox.focus_force()
        self.labelCategoie.pack()
        self.listbox.pack()
        self.frameMenuSelect.pack()
        self.root.mainloop()

    def selection(self,eventKey):
        if(eventKey.keycode == 13):
            self.indice = self.listbox.curselection()[0]
            self.idMap = self.listbox.get(self.indice)[7]
            self.root.destroy()

    def envoieMap(self):
        return dtoOracleMap.DtoMap().getMapChoose(self.idMap)