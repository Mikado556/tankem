# -*- coding: utf-8 -*-
from direct.showbase.ShowBase import ShowBase
from panda3d.core import * #Pour la classe Vec3
from direct.gui.OnscreenText  import OnscreenText 
import random


import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\..\\..\\..\\tools"
sys.path.append(cheminFichierMain)

from InterfaceMenuAthentification import *


class MyApp(ShowBase):
    def __init__(self, donnees):
        ShowBase.__init__(self)

        J1, J2 = donnees

        #Fait appara�tre le widget qui sert � placer les noeuds.
        self.camera.place()
  
        #Place la cam�ra
        self.camera.setPos(0,-844,324)
        self.camera.setHpr(356,338,0)

        t1 = self.hex_to_rgb(J1[1])
        t2 = self.hex_to_rgb(J2[1])

        self.drawTank(200, -90, t1)
        self.drawTank(-80, 90, t2)

        base.setBackgroundColor(0.20,0.28,0.17)
        
        textObject = OnscreenText(text = J1[0], pos = (-0.6, 0.2), scale = 0.09)
        textObject = OnscreenText(text = J2[0], pos = (0.6, 0.2), scale = 0.09)

        textObject = OnscreenText(text = 'VS', pos = (0, 0.02), scale = 0.4)
    

    def drawTank(self, x, h, couleur):
        #Charge le tank
        self.tank = loader.loadModel("../../asset/Tank/tank")
        self.tank.setScale(50,50,50)
        self.tank.setPos(Vec3(x,411,-40))
        self.tank.setHpr(180,0,0)

        self.tank.setColorScale(couleur[0]/255,couleur[1]/255,couleur[2]/255,1)

        posArrivee = self.tank.getPos() + Point3(0,-500,0)
        posDepart = self.tank.getPos()
        
        intervalAvance = self.tank.posInterval(5,posArrivee,startPos = posDepart,blendType = "easeOut")

        self.tank.reparentTo(render)
        intervalAvance.start()
        
        hprArrivee = self.tank.getHpr() + Point3(h,0,0)
        hprDepart = self.tank.getHpr()
        intervalTourne = self.tank.posHprInterval(12,posArrivee,hprArrivee,startPos = posArrivee,blendType = "easeOut")
        intervalTourne.start()

    def hex_to_rgb(self,value):
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))


M = MenuAuthentification()

app = MyApp(M.getDonnee())
app.run()
