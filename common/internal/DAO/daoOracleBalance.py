## -*- coding: utf-8 -*-
import cx_Oracle
import csv
import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\..\\DTO"
sys.path.append(cheminFichierMain)
from DTO import *

class DaoOracle():
    def __init__(self):
        self.dictionnaire={}

    def seConnecter(self):
        try:
            self.con = cx_Oracle.connect("e1317119","123","10.57.4.60/DECINFO.edu") 
            csv.register_dialect("Pipe",delimiter=";") 
            self.cur = self.con.cursor() # le curseur
            return True
        except cx_Oracle.Error as error:
            print("erreur dans la connexion, voici l'erreur : " + str(error)) 
            return False

    # si la connexion marche , elle lit la base de données
    def readBalance(self):
        try:
            tableParamTank = self.cur.execute('SELECT * FROM ParametreTank')
            for key in tableParamTank.fetchall():
                self.dictionnaire[key[0]] = key[1]
            
            tableMessage = self.cur.execute('SELECT * FROM MessageDuJeu')
            for key in tableMessage.fetchall():
                self.dictionnaire[key[0]] = key[1]
            #print self.dictionnaire
            return self.dictionnaire  
        except cx_Oracle.DatabaseError  as error:
            print(error)          

    # si la connexion échoue, on utilisera le fichier ParametreTankDefault.csv 
    # dans le dossier tools
    def readBalanceDefault(self):
        nameFile = open("..\\..\\tools\\ParametreTankDefault.csv",'rb')
        csv.register_dialect("Pipe",delimiter=";") 
        reader = csv.reader(nameFile,"Pipe")

        for i in reader:
            self.dictionnaire[i[0]] = i[1]
        #print(self.dictionnaire["Vitesse des chars"])
        print("la commande est fini correctement")
        #print self.dictionnaire
        return self.dictionnaire

    
    # modifie la base de données 
    def updateBalance(self,DonneUpdate):
        # donneesCSV  est une liste
        donneesCSV = DonneUpdate
        # on initialise des tuples vides
        colonnesTank = ()
        valeursTank = ()
        colonnesMess = ()
        valeursMess = ()

        # on concatène les tuples
        for i in range(0,len(donneesCSV)):
            if i < 19:
                colonnesTank+=(donneesCSV[i][0],)
                valeursTank+=(donneesCSV[i][1],)
            else:
                colonnesMess+=(donneesCSV[i][0],)
                valeursMess+=(donneesCSV[i][1],)
    
        # on crée les statments
        statementTank=""
        for i in range(0,len(colonnesTank)-1):
            if i == 17:
                statementTank += colonnesTank[i] + " = " + valeursTank[i] 
            else:
                statementTank += colonnesTank[i] + " = " + valeursTank[i] + ","

        statementMess=""
        for i in range(0,len(colonnesMess)-1):
            if i == 17:
                statementMess += colonnesMess[i] + " = " + valeursMess[i] 
            else:
                statementMess += colonnesMess[i] + " = " + valeursMess[i] + ","

        # on fait la modification des données à cx_Oracle
        try:
            self.cur.execute("UPDATE ParametreTank SET " + statementTank)
            self.cur.execute("UPDATE ParametreTank SET " + statementMess)
            print "YES RÉUSSI"
            self.commit()
        except cx_Oracle.DatabaseError  as error:
            print(error)
        
        
    # fermer le curseur et la connection
    def close(self): 
        self.cur.close()
        self.con.close()

    # faire un commit 
    def commit(self): 
        self.cur.commit();