#-*- coding: utf-8 -*-
import cx_Oracle
import ConnexionBD

class DAO_Oracle_Statistique():
    def __init__(self):
        self.con = ConnexionBD.ConnexionOracle().seConnecter()
        self.curseur = self.con.cursor()

    def createDommage(self, pseudonyme, niveau, arme, dommage, etat, posX, posY):
        requeteSQL = 'INSERT INTO TableDommage(ID_USER, ID_NIVEAU, ID_ARME, DOMMAGE, ETAT, POSX, POSY) VALUES ((SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = :1),:2, (SELECT IDARME FROM TableTypeArme WHERE NOM = :3),:4,:5,:6,:7)'
        donneeBind = [pseudonyme, niveau, arme, dommage, etat, posX, posY]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")


    def createTempsPasserSurUneCase(self, pseudonyme, niveau, temps, posX, posY):
        requeteSQL = 'INSERT INTO TableTemps(ID_UTILISATEUR, ID_NIVEAU, TEMPSCASE, POSX, POSY) VALUES ((SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = :1), :2,:3,:4,:5)'
        donneeBind = [pseudonyme, niveau, temps, posX, posY]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")


    def createCoupFeu(self, pseudonyme, arme, nbCoupFeu):
        requeteSQL = 'INSERT INTO TableCoupFeu(ID_USER, ID_ARME, NBCOUPFEU) VALUES ((SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = :1),(SELECT IDARME FROM TableTypeArme WHERE NOM = :2),:3)'
        donneeBind = [pseudonyme, arme, nbCoupFeu]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")

    def createStatistiquePartie(self, pseudonyme1, pseudonyme2, STATUT1, STATUT2):
        requeteSQL = 'INSERT INTO TableStatistique(ID_USER1, ID_USER2, STATUT1, STATUT2) VALUES ((SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = :1),(SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = :2),:3,:4)'
        donneeBind = [pseudonyme1, pseudonyme2, STATUT1, STATUT2]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")