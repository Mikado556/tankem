import cx_Oracle
import ConnexionBD
from collections import OrderedDict

class DaoMap():
    def __init__ (self):
        self.con = ConnexionBD.ConnexionOracle().seConnecter()
        self.cur = self.con.cursor()
        self.dictPosBloc = {}
        self.dictGrosseurMap = {}
        self.dictAReturn = {}
        self.dictAReturnTrie = []
        self.dictionnaire={}
        self.dictDelaiMinMax = {}

    def readMap(self):
        try:
            self.cur.execute('SELECT * FROM TableNiveau')
            tableNiveau = self.cur.fetchall()
            return tableNiveau
            #for key in tableNiveau.fetchall():
                #self.dictionnaire[key[7]] = key
            #print(self.dictionnaire)
            #execute = "SELECT nom FROM TableUtilisateur WHERE idUtilisateur = '" + str(self.dictionnaire[6])+"'"
            #TableUtilisateur = self.cur.execute(execute)

            #self.dictionnaire[][6] = TableUtilisateur[0]
            #return self.dictionnaire  
        except cx_Oracle.DatabaseError  as error:
            print(error) 

    def readMapChoose(self,idMap):
        try:
            execute = "SELECT idPosBloc,posX,posY,bloc FROM TablePositionBloc WHERE idMap = '" +  str(idMap) + "'"
            TablePositionBloc = self.cur.execute(execute)
            for key in TablePositionBloc.fetchall():
                self.dictPosBloc[key[0]] = key

            execute = "SELECT * FROM TableMapJeu WHERE idMap = '" +  str(idMap) + "'"
            TableMapJeu = self.cur.execute(execute)
            for key in TableMapJeu.fetchall():
                self.dictGrosseurMap[key[0]] = key

            #execute = "SELECT idMap,delaiMinimun,delaiMaximun FROM TableNiveau WHERE idMap = '" +  str(idMap) + "'"
            #TableNiveau = self.cur.execute(execute)
            #for key in TablePositionBloc.fetchall():
            #    self.dictDelaiMinMax[key[0]] = key

            
            self.dictAReturn = {"GX":self.dictGrosseurMap[idMap][1],"GY":self.dictGrosseurMap[idMap][2]}
            #self.dictAReturn = {"Min":self.dictDelaiMinMax[idMap][1],"Max":self.dictDelaiMinMax[idMap][2]}
            for key in self.dictPosBloc:
                self.dictAReturn[key] = self.dictPosBloc[key]

            self.trie = sorted(self.dictAReturn)
            print self.trie
            while(len(self.trie) != 0):
                #or self.trie[0] == "Min" or self.trie[0] == "Max"
                if self.trie[0] == "GX" or self.trie[0]  == "GY":
                    valueRecherche = self.trie[0]
                else:
                    valueRecherche = int(self.trie[0])
                self.dictAReturnTrie.append(self.dictAReturn[valueRecherche])
                self.trie.remove(valueRecherche)
            print self.dictAReturnTrie
            return self.dictAReturnTrie

        except cx_Oracle.DatabaseError  as error:
            print(error)

    def createMap(self,dtoUpdate):
        listDonnee = dtoUpdate[0]
        listPos = dtoUpdate[1]
        #listDonne contient; nom,grosseurX,grosseurY,titre,delaiminimun,delaimaiximun,statut
        try:
            execute = "INSERT INTO TableUtilisateur(nom) VALUES ('" + listDonnee[0]+ "')"
            self.cur.execute(execute)
            self.con.commit()
            execute = "INSERT INTO TableMapJeu (grosseurX,grosseurY) VALUES ('"+str(listDonnee[1])+"','"+str(listDonnee[2])+"')"
            self.cur.execute(execute)
            self.con.commit()
            execute = "SELECT idUtilisateur FROM TableUtilisateur WHERE nom = '"+listDonnee[0]+"'"
            TableUtilisateur = self.cur.execute(execute)
            idUti = TableUtilisateur.fetchone()[0]
            execute = "SELECT idMap FROM TableMapJeu"
            TableMapJeu = self.cur.execute(execute)
            idMap = TableMapJeu.fetchall()[-1][0]
            execute = "INSERT INTO TableNiveau(titre,delaiMinimun,delaiMaximun,statut,idUtilisateur,idMap) VALUES('"+listDonnee[3]+"','"+str(listDonnee[4])+"','"+str(listDonnee[5])+"','"+listDonnee[6]+"','"+str(idUti)+"','"+str(idMap)+"')"
            self.cur.execute(execute)
            self.con.commit()
            for tupple in listPos:
                execute = "INSERT INTO TablePositionBloc(idMap,posX,posY,bloc) VALUES ('"+str(idMap)+"','"+str(tupple[0])+"','"+str(tupple[1])+"','"+str(tupple[2])+"')"
                self.cur.execute(execute)
                self.con.commit()
        except cx_Oracle.DatabaseError  as error:
            print(error)