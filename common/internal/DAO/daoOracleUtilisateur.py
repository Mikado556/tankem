#-*- coding: utf-8 -*-
import cx_Oracle
import ConnexionBD

class DAO_Oracle_Utilisateur():
    def __init__(self):
        self.con = ConnexionBD.ConnexionOracle().seConnecter()
        self.curseur = self.con.cursor()
        self.dictionnaire = {}

#Utilisateur
    def lireCouleurUtilisateur(self, pseudonyme): #ON UTILISE CELLE-CI SUR TANKEM!
        try:
            self.requeteSQL = 'SELECT couleurTank FROM TableUtilisateur WHERE pseudonyme = :1'
            donneeBind = [pseudonyme]
            self.curseur.execute(self.requeteSQL, donneeBind)
            self.couleur = self.curseur.fetchone()
            return self.couleur
        except cx_Oracle.DatabaseError  as error:
            print(error) 

    def lireMotPasseUtilisateur(self, pseudonyme):
        try:
            self.requeteSQL = 'SELECT motDePasse FROM TableUtilisateur WHERE pseudonyme = :1'
            donneeBind = [pseudonyme]
            self.curseur.execute(self.requeteSQL, donneeBind)
            mdp = self.curseur.fetchone()
            print mdp
            return mdp
        except cx_Oracle.DatabaseError  as error:
            print(error) 


#NbFoisNiveauJouer

    def lireAdditionnerNbFoisNiveauJoueur(self): #ON UTILISE CELLE-CI DANS TANKEM POUR LE TOTAL
        requeteSQL = 'SELECT SUM(NbFoisJouer) FROM tableAssoNbFoisJouer'
        self.curseur.execute(requeteSQL)
        total = self.curseur.fetchall()[0][0]
        if(total == None):
            total = 0
        print(total)
        return total

    def lireNbFoisJouer(self, id_niveau, id_utilisateur):
        requeteSQL = 'SELECT NbFoisJouer FROM tableAssoNbFoisJouer WHERE idNiveau=:1 AND idUtilisateur=:2'
        donneeBind = [id_niveau, id_utilisateur]
        self.curseur.execute(requeteSQL, donneeBind)
        resultat = self.curseur.fetchall()
        return resultat

    def modifierNbFoisNiveauJouer(self, id_niveau, id_utilisateur):
        requeteSQL = 'SELECT NbFoisJouer FROM tableAssoNbFoisJouer WHERE idNiveau=:1 AND idUtilisateur=:2'
        donneeBind = [id_niveau, id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        nbFoisJouer = self.curseur.fetchall()[0][0]
        nbFoisJouer += 1
        requeteSQL = 'UPDATE tableAssoNbFoisJouer SET NbFoisJouer = :1 WHERE idNiveau=:2 AND idUtilisateur=:3'
        donneeBind = [nbFoisJouer, id_niveau, id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")

    def creerNbFoisNiveauJouer(self, id_niveau, id_utilisateur, nbFoisJouer):
        requeteSQL = 'INSERT INTO tableAssoNbFoisJouer (idNiveau,idUtilisateur,NbFoisJouer) VALUES (:1,:2,:3)'
        donneeBind = [id_niveau, id_utilisateur, nbFoisJouer]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")

    def ajouterNbFoisJouer(self, id_niveau, id_utilisateur):  # TANKEM UTILISERA CELUI-CI
        if self.verifierExistenceNbFoisNiveauJouer(id_niveau, id_utilisateur):
            self.modifierNbFoisNiveauJouer(id_niveau, id_utilisateur)
        else:
            self.creerNbFoisNiveauJouer(id_niveau, id_utilisateur,1)

    def verifierExistenceNbFoisNiveauJouer(self, id_niveau, id_utilisateur):
        existe = self.lireNbFoisJouer(id_niveau, id_utilisateur)
        if(not existe):
            return False
        else:
            return True

#FavorisNiveau      
    def creerFavorisNiveau(self, id_niveau, id_utilisateur):
        requeteSQL = 'INSERT INTO tableAssoFavoris(idNiveau, idUtilisateur) VALUES(:1,:2)'
        donneeBind = [id_niveau, id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")    

    def supprimerFavorisNiveau(self, id_niveau, id_utilisateur):
        requeteSQL = 'DELETE FROM tableAssoFavoris WHERE idNiveau=:1 AND idUtilisateur=:2'
        donneeBind = [id_niveau, id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")

    def lireFavorisNiveau(self, id_utilisateur):
        requeteSQL = 'SELECT idNiveau FROM tableAssoFavoris WHERE idUtilisateur=:1'
        donneeBind = [id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        resultat = self.curseur.fetchall()
        print(resultat)
        return resultat

#Arme d'un utilisateur
    def creerArmeUtilisateur(self, id_Arme, id_utilisateur):
        requeteSQL = 'INSERT INTO tableAssoArme (idArme,idUtilisateur) VALUES(:1,:2)'
        donneeBind = [id_Arme,id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")

    def supprimerArmeUtilisateur(self,id_Arme,id_utilisateur):
        requeteSQL = 'DELETE FROM tableAssoArme WHERE idArme=:1 AND idUtilisateur=:2'
        donneeBind = [id_Arme,id_utilisateur]
        self.curseur.execute(requeteSQL,donneeBind)
        self.curseur.execute("commit")

    #Modifier voir les armwe de lutilisateur!!!!
    def lireTypeArmeUtilisateur(self, pseudonyme):
        self.listArme = []
        requeteSQL = 'SELECT idUtilisateur FROM TableUtilisateur WHERE pseudonyme=:1'
        donneeBind = [pseudonyme]
        self.curseur.execute(requeteSQL,donneeBind)
        idUtil = self.curseur.fetchall()[0][0]
        requeteSQL = 'SELECT idArme FROM tableAssoArme WHERE idUtilisateur=:1'
        donneeBind = [idUtil]
        self.curseur.execute(requeteSQL,donneeBind)
        nom = self.curseur.fetchall()
        for x in xrange(0,len(nom)):
            idTypeArme = nom[x][0]
            requeteSQL = 'SELECT nom FROM tableTypeArme WHERE idArme=:1'
            donneeBind = [idTypeArme]
            self.curseur.execute(requeteSQL, donneeBind)
            self.nomArme = self.curseur.fetchall()[0][0]
            self.listArme.append(self.nomArme)
        return self.listArme


