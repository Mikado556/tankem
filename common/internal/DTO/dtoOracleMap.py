import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\.."
sys.path.append(cheminFichierMain)
from DAO import daoOracleMap

class DtoMap():
    def __init__(self):
        pass
    def getMapExistant(self):
        return daoOracleMap.DaoMap().readMap()

    def getMapChoose(self,idMap):
        return daoOracleMap.DaoMap().readMapChoose(idMap)

    def setUpdateMap(self, listDonne, listPos):
        returnUpdate = (listDonne,listPos)
        daoOracleMap.DaoMap().createMap(returnUpdate)