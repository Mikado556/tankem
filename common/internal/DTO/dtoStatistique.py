# -*- coding: utf-8 -*-
import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\.."
sys.path.append(cheminFichierMain)
from DAO import daoOracleStatistique

class DTO_Statistique():
    def __init__(self):
        pass

    def setDommage(self, pseudonyme, niveau, arme, dommage, etat, posX, posY):
        daoOracleStatistique.DAO_Oracle_Statistique().createDommage(pseudonyme, niveau, arme, dommage, etat, posX, posY)

    def setTempsPasserSurUneCase(self, pseudonyme, niveau, temps, posX, posY):
        daoOracleStatistique.DAO_Oracle_Statistique().createTempsPasserSurUneCase(pseudonyme, niveau, temps, posX, posY)

    def setCoupFeu(self, pseudonyme, arme, nbCoupFeu):
        daoOracleStatistique.DAO_Oracle_Statistique().createCoupFeu(pseudonyme, arme, nbCoupFeu)


    

