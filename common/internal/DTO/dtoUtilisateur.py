# -*- coding: utf-8 -*-
import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\.."
sys.path.append(cheminFichierMain)
from DAO import daoOracleUtilisateur

class DTO_Utilisateur:
    def __init__(self):
    	pass

    def getCouleurUtilisateur(self, couleurTank):
        return daoOracleUtilisateur.DAO_Oracle_Utilisateur().lireCouleurUtilisateur(couleurTank)

    def getMotDePasse(self, pseudonyme):
        return daoOracleUtilisateur.DAO_Oracle_Utilisateur().lireMotPasseUtilisateur(pseudonyme)

    def getTotalNdFoisJouer(self):
        return daoOracleUtilisateur.DAO_Oracle_Utilisateur().lireAdditionnerNbFoisNiveauJoueur()

    def getFavorisUtilisateur(self, id_utilisateur):
        return daoOracleUtilisateur.DAO_Oracle_Utilisateur().lireFavorisNiveau(id_utilisateur)

    def getLesArmesUtilisateur(self, pseudonyme):
        return daoOracleUtilisateur.DAO_Oracle_Utilisateur().lireTypeArmeUtilisateur(pseudonyme)

    def setLeNbFoisJouer(self, id_niveau, id_utilisateur):
        daoOracleUtilisateur.DAO_Oracle_Utilisateur().ajouterNbFoisJouer(id_niveau, id_utilisateur)

    def setCreerFavoris(self, id_niveau, id_utilisateur):
        daoOracleUtilisateur.DAO_Oracle_Utilisateur().creerFavorisNiveau(id_niveau, id_utilisateur)

    def setSupprimerFavoris(self, id_niveau, id_utilisateur):
        daoOracleUtilisateur.DAO_Oracle_Utilisateur().supprimerFavorisNiveau(id_niveau, id_utilisateur)

    def setCreerArmeUtilisateur(self, id_Arme, id_utilisateur):
        daoOracleUtilisateur.DAO_Oracle_Utilisateur().creerArmeUtilisateur(id_Arme, id_utilisateur)

    def setSupprimerArmeUtilisateur(self, id_Arme, id_utilisateur):
        daoOracleUtilisateur.DAO_Oracle_Utilisateur().supprimerArmeUtilisateur(id_Arme, id_utilisateur)
    

