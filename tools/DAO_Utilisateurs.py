 #-*- coding:utf-8 -*-
import cx_Oracle
import sys
import Tkinter
import tkMessageBox



class Dao_Utilisateurs():
    def __init__(self):
        self.con = cx_Oracle.connect("e0853620", "2222", "10.57.4.60/DECINFO.edu")
        self.curseur = self.con.cursor()
        
    def executer(self,element):
        try:
           self.curseur.execute(None,{'element':element})
           reponse = self.curseur.fetchone()
           return reponse
        except cx_Oracle.DatabaseError as e:
           #tupple error
           error, = e.args
           tkMessageBox.showinfo("connection_invalide","Probleme de connection.Configuration par default utilise")
           print (u"Une erreur de commande")
           print (error.code) #Rechercher sur google le code d'erreur
           print (error.message) #Plus de détails
           print (error.context) #Te donne la fonction qui a planté
           return False
       
    def recuperer_motdepasse(self,value):
        try:
          c=self.curseur.execute("Select MOTPASS from UTILISATEURS WHERE ID_USER=:1",(value,))
          row=c.fetchone()
          d=row[0]   
          return d
        except cx_Oracle.Error as e:
          print(e)
          self.con.rollback()
    
    def recuperer_armes(self,value):
        try:
          c=self.curseur.execute("Select GUN from STOCKGUN WHERE ID=:1",(value,))
          row=c.fetchall()  
          id_list = []
          for index in range(len(row)):
              id_list.append(row[index][0])
          return  id_list
        except cx_Oracle.Error as e:
          print(e)
          self.con.rollback()
      