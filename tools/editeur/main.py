# -*- coding:utf-8 -*-
from Tkinter import *
import ttk
import tkMessageBox as popup

import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\..\\..\\common\\internal"
sys.path.append(cheminFichierMain)
from DTO import *

class editeur():
    def __init__(self):
        self.dto = dtoOracleMap.DtoMap()
        self.root = Tk() # créer ma fenêtre
        self.root.title("Editeur de map")
        self.cases=[] # contient les labels
        self.width = 500
        self.height = 350
        self.dataList = [] # contient nom, gx, gy, titre, temps minimum et maximum, statut
        self.postList = [] # contient posx , posy, type de bloc (plancher,mur,...)
        # taille maximale de la matrice
        self.xMax = 0
        self.yMax = 0
        self.menu()
        self.center_window(self.root,self.width,self.height) # centrer la fenetre
        self.root.mainloop() # run

#############################################################################
    def center_window(self,fen,width, height):
        # get screen width and height
        screen_width = fen.winfo_screenwidth()
        screen_height = fen.winfo_screenheight()
        # calculate position x and y coordinates
        x = (screen_width/2) - (width/2)
        y = (screen_height/2) - (height/2)
        fen.geometry('%dx%d+%d+%d' % (width, height, x, y))
#############################################################################

    def menu(self):
        self.frameMenu = Frame(self.root,width=self.width,height=self.height)
        # Menu principal
        lblTitle = Label(self.frameMenu,text="Menu principal",bd=2,relief=GROOVE)
        btnCreateLvl = Button(self.frameMenu,text="Créer un niveau",command=self.createLevel)
        btnQuitter = Button(self.frameMenu,text="Quitter",command=self.quitter)
        # on montre les widgets
        lblTitle.grid(row=0,column=0,pady=20)
        btnCreateLvl.grid(row=1,column=0,pady=20)
        btnQuitter.grid(row=2,column=0)
        self.frameMenu.pack()

    def createLevel(self):
        self.frameMenu.forget()
        # champs
        self.frameCreateLvl = Frame(self.root)
        lblTitle = Label(self.frameCreateLvl,text="titre")
        lblDimension = Label(self.frameCreateLvl,text="dimension")
        lblX = Label(self.frameCreateLvl,text="x")
        lblY = Label(self.frameCreateLvl,text="y")
        self.etitle = Entry(self.frameCreateLvl)
        self.eX = Entry(self.frameCreateLvl)
        self.eY = Entry(self.frameCreateLvl)
        self.box_value = StringVar()
        self.box = ttk.Combobox(self.frameCreateLvl, textvariable=self.box_value, 
                                state='readonly',value= ('Public', 'Privée', 'Inactif'))
        self.box.current(0) # valeur par défaut public
        lbltime = Label(self.frameCreateLvl,text="délais (minimum et maximale)")
        self.etimeMin = Entry(self.frameCreateLvl)
        self.etimeMin.insert(0, "2")
        self.etimeMin.bind("<Button-1>",lambda a: self.etimeMin.delete(0, END))
        self.etimeMax = Entry(self.frameCreateLvl)
        self.etimeMax.insert(0, "20")
        self.etimeMax.bind("<Button-1>",lambda a: self.etimeMax.delete(0, END))
        btnNext = Button(self.frameCreateLvl,text="suivant",command=self.verificationChamps)
        # on montre les widgets
        lblTitle.grid(row=0,column=0,pady=20)
        lblDimension.grid(row=1,column=0)
        lblX.grid(row=2,column=0)
        lblY.grid(row=3,column=0)
        self.etitle.grid(row=0,column=1)
        self.eX.grid(row=2,column=1)
        self.eY.grid(row=3,column=1)    
        lbltime.grid(row=4,column=0,pady=15)  
        self.etimeMin.grid(row=5,column=1,pady=10)
        self.etimeMax.grid(row=6,column=1,pady=10)
        self.box.grid( row=7,column=1,pady=10)
        btnNext.grid(row=8,column=2)
        self.frameCreateLvl.pack()

    def quitter(self):
        exit()

    def verificationChamps(self):
        titleMap = self.etitle.get()
        x = self.eX.get()
        y = self.eY.get()
        self.xMax = int(x)
        self.yMax = int(y)
        statut = self.box.get()
        self.timeMin = self.etimeMin.get() # par défaut c'est 2
        self.timeMax = self.etimeMax.get() # par défaut c'est 20

        if len(titleMap) > 0 and len(x) > 0 and len(y) > 0: # s'il y a un titre, un x et un y
            if int(x) >= 6  and int(x) <= 12 and int(y) >= 6 and int(y) <=12: # si compris entre 6 et 12
                self.editerMap(titleMap,x,y,statut,self.timeMin,self.timeMax)
            else:
                popup.showinfo("Message aide","Taille invalide ! seulement entre 6 et 12")
        else:
            popup.showinfo("Message aide","Titre ou dimension manquant(e)")        

#############################################################################
    # cette fonction va générer le cadrillage pour ensuite personnaliser
    # langage tkinter
    def editerMap(self,titre,x,y,statut,timeMin,timeMax):
        print(titre ,(x,y), statut)
        self.cases=[] # on réinitialise la liste
        self.fenEditer = Tk() # créer une nouvelle fenêtre
        self.fenEditer.title("Personnalisation de la carte")
        self.center_window(self.fenEditer,1400,400)
        # menu
        menubar = Menu(self.fenEditer)
        # action
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Save",command=self.save)
        filemenu.add_separator()
        filemenu.add_command(label="Exit",command=self.quitter)
        menubar.add_cascade(label="Action", menu=filemenu)
        # help
        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="control panel (F1)",command=self.controlKeys)
        helpmenu.add_command(label="Read me ! (F2)",command=self.openReadMe)
        menubar.add_cascade(label="Help", menu=helpmenu)
        # display the menu
        self.fenEditer.config(menu=menubar)

        ########################################################
        self.framePrincipal = Frame(self.fenEditer) # englobe les autres frames
        height = int(y)
        width = int(x)
        # frame contenant le quadrillage
        self.frameMap = Frame(self.framePrincipal)
        for i in range(height): #Rows
            for j in range(width): #Columns
                label=Label(self.frameMap,text="plancher",width=12,bd=1, relief=GROOVE,bg="navy",fg="white")
                label.grid(row=i, column=j)
                self.cases.append(label)

        self.cases[0].config(bg="red") # curseur est ici
        #for i in cases:
            #print(i.grid_info()["row"] + "," + i.grid_info()["column"]) # affiche les coordonnées des widgets
        
        # frame contenant le titre, le nom du créateur et autres informations
        self.frameDonnees = Frame(self.framePrincipal)
        lblTitle = Label(self.frameDonnees,text="Titre\n" + titre)
        lblDimension = Label(self.frameDonnees,text="Dimension : " + x + " x " + y)
        lblStatut = Label(self.frameDonnees,text="Statut : " + statut)
        lblNom = Label(self.frameDonnees,text="Entrez votre nom")
        self.eNomCreateur = Entry(self.frameDonnees)
        lblLegende = Label(self.frameDonnees,bg="white",justify=LEFT,text="Légende\nbleu : plancher\nvert : mur fixe\njaune :  mur animé verticalement\nmauve : mur animé verticalement inversé")
        lblDelais = Label(self.frameDonnees,text="Délais d'apparation\nentre("+timeMin+","+timeMax+")sec")
        # on montre les widgets
        lblTitle.grid(row=0,column=0)
        lblDimension.grid(row=1,column=0)
        lblStatut.grid(row=2,column=0)
        lblNom.grid(row=3,column=0)
        self.eNomCreateur.grid(row=4,column=0)
        lblLegende.grid(row=5,column=0,pady=10,padx=10)
        lblDelais.grid(row=6,column=0)
        # on pack les frames
        self.frameMap.pack_propagate(False)
        self.frameMap.pack(side=RIGHT)
        self.frameDonnees.pack_propagate(False)
        self.frameDonnees.pack(side=LEFT)
        self.framePrincipal.pack(expand=1)
        self.fenEditer.bind("<Key>", self.pressedKey) # écoute le clavier

    def openReadMe(self):
        print("readme")

    def controlKeys(self):
        lblKeyboard =  Label(self.frameDonnees,bg="bisque",text="Touches directionnelles\n haut : W\ngauche : A\nbas : S\ndroite : D")
        lblKeyboard.grid(row=7,column=0)

    def save(self): # 
        if popup.askquestion("Sauvegarde", "Voulez-vous sauvegarder ?") != "no":
            print('je Sauvegarde')
            nom = self.eNomCreateur.get()
            gx = self.eX.get()
            gy = self.eY.get()
            titre = self.etitle.get()
            statut = self.box.get()
            if len(nom) > 0 :
                self.submit(nom,gx,gy,titre,self.timeMin,self.timeMax,statut)
            else:
                popup.showinfo("message d'erreur","aucun nom de créateur n'a été mentionné")

    def pressedKey(self,event):
        print "keycode :" + chr(event.keycode)
        lettre  = chr(event.keycode)
        # pos curseur
        pos = 0
        for i in self.cases:
            if i.cget('bg') == "red":
                break;
            pos+=1

        cursx = self.cases[pos].grid_info()["column"] # curseur x
        cursy = self.cases[pos].grid_info()["row"] # curseur y
        print (cursx + "," + cursy)
        # le deplacement additionnel
        addX = 0
        addY = 0

        if(lettre == 'D') : # droite : D
            if self.deplacementCurseur(cursx,cursy,1,0,lettre):
                self.changerCaseCurseur(pos,self.cases[pos].cget('text'))
                addX+=1
        elif (lettre == 'S'): # bas : S
            if self.deplacementCurseur(cursx,cursy,0,1,lettre):
                self.changerCaseCurseur(pos,self.cases[pos].cget('text'))
                addY+=1
        elif (lettre== 'A'): # Gauche A
            if self.deplacementCurseur(cursx,cursy,1,0,lettre):
                self.changerCaseCurseur(pos,self.cases[pos].cget('text'))
                addX-=1
        elif(lettre == 'W'): # haut : W
            if self.deplacementCurseur(cursx,cursy,0,1,lettre):
                self.changerCaseCurseur(pos,self.cases[pos].cget('text'))
                addY-=1
        elif(lettre == ' '): # Espace
            self.changerTypeBloc(pos,self.cases[pos].cget('text'))
        elif(lettre == 'F1'): # F1 : control keys 
            self.controlKeys()
        elif(lettre == 'F2'): # F2 : open read me 
            self.openReadMe()

        for i in self.cases:
            print(i.grid_info()["column"]+"="+ str(int(cursx)+addX))
            if int(i.grid_info()["column"]) == int(cursx)+addX and int(i.grid_info()["row"]) == int(cursy)+addY:
                i.config(bg="red") # rechange la couleur du curseur

    def deplacementCurseur(self,cursx,cursy,x,y,lettre):
        valide = False
        print("curs " + str(int(cursx)+x) +", max "+ str(self.xMax-1))
        if int(cursx)+x < self.xMax and lettre == 'D': # droite
            print("d : cursx" + str(int(cursx)+x) + "," +str(int(self.xMax)-1) )
            valide = True
        elif int(cursx)-x >= 0 and lettre == 'A': # gauche
            print("g")
            valide = True
        elif int(cursy)+y < self.yMax and lettre == 'S' : # bas
            print("b")
            valide = True
        elif int(cursy)-y >= 0 and lettre == 'W': # haut
            print("h")
            valide = True

        return valide

    def changerCaseCurseur(self,pos,nomBloc):
        if nomBloc == "plancher":
            self.cases[pos].config(bg="navy")
        elif nomBloc == "mur fixe":
            self.cases[pos].config(bg="dark green")
        elif nomBloc == "mur a.v":
            self.cases[pos].config(bg="goldenrod",text="mur a.v")
        elif nomBloc == "mur a.v.i":
            self.cases[pos].config(bg="purple3",text="mur a.v.i")

    def changerTypeBloc(self,pos,nomBloc):
        if nomBloc == "plancher":
            self.cases[pos].config(bg="dark green",text="mur fixe")
        elif nomBloc == "mur fixe":
            self.cases[pos].config(bg="goldenrod",text="mur a.v")
        elif nomBloc == "mur a.v":
            self.cases[pos].config(bg="purple3",text="mur a.v.i")
        elif nomBloc == "mur a.v.i":
            self.cases[pos].config(bg="navy",text="plancher")

    def submit(self,nom,gx,gy,titre,tmin,tmax,statut):
        #print(nom + "," + str(gx) + "," + str(gy) + "," +  titre + "," + str(tmin) + "," + str(tmax) + "," + statut)
        # ajout dans la liste
        self.dataList.append(nom)
        self.dataList.append(gx)
        self.dataList.append(gy)
        self.dataList.append(titre)
        self.dataList.append(tmin)
        self.dataList.append(tmax)
        self.dataList.append(statut)
        # ajout dans tuple
        for i in self.cases:
            self.postList.append((i.grid_info()["column"],i.grid_info()["row"],i.cget('text'),))
            print (i.grid_info()["column"],i.grid_info()["row"],i.cget('text'),)
        print self.postList
        self.dto.setUpdateMap(self.dataList, self.postList)



        

e = editeur()
