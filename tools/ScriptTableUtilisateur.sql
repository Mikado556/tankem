--GENERATED
--La table Utilisateur existe deja dans le script-createNiveaux.sqls
--Docn je ferai un ALter table de la table

DROP TABLE tableQuestion CASCADE CONSTRAINTS;

CREATE TABLE tableQuestion
(
	idQuestion NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	Question VARCHAR2(255)
);

ALTER TABLE TableUtilisateur ADD nom VARCHAR2(255);
ALTER TABLE TableUtilisateur ADD prenom VARCHAR2(255);
ALTER TABLE TableUtilisateur ADD motDePasse VARCHAR2(255);
ALTER TABLE TableUtilisateur ADD couleurTank VARCHAR2(255);
ALTER TABLE TableUtilisateur ADD idQuestionA NUMBER;
ALTER TABLE TableUtilisateur ADD CONSTRAINT fk_IdQuesiotnA FOREIGN KEY (idQuestionA) REFERENCES tableQuestion(idQuestion);
ALTER TABLE TableUtilisateur ADD reponseA VARCHAR2(255);
ALTER TABLE TableUtilisateur ADD idQuestionB NUMBER;
ALTER TABLE TableUtilisateur ADD CONSTRAINT fk_IdQuesiotnB FOREIGN KEY (idQuestionB) REFERENCES tableQuestion(idQuestion);
ALTER TABLE TableUtilisateur ADD reponseB VARCHAR2(255);
ALTER TABLE TableUtilisateur ADD visibilite NUMBER;

INSERT INTO  tableQuestion(Question) VALUES ('Quel est le nom de votre mere?');
INSERT INTO  tableQuestion(Question) VALUES ('Quel est le nom de votre pere?');
INSERT INTO  tableQuestion(Question) VALUES ('Quel etait le nom de votre premier animal de compagnie?');
INSERT INTO  tableQuestion(Question) VALUES ('Quel est la marque de votre premiere voiture?');
INSERT INTO  tableQuestion(Question) VALUES ('Quel est le nom de votre premier amour?');

UPDATE TableUtilisateur SET  nom = 'Mr.D', prenom = 'None', motDePasse = '$2a$12$mvXmdry7Yg5dO5TMUmmNEO9iG7pGXSGQwFLqFBh8URbYuvC/Z3C7y', couleurTank = 'f00', idQuestionA = '1', reponseA = 'Mama', idQuestionB = '2', reponseB = 'Papa', visibilite = 1 WHERE pseudonyme = 'Admin';

INSERT INTO  TableUtilisateur (nom, prenom, pseudonyme, motDePasse, couleurTank, idQuestionA, reponseA, idQuestionB, reponseB, visibilite) VALUES ('John', 'Di', 'C','$2a$12$mvXmdry7Yg5dO5TMUmmNEO9iG7pGXSGQwFLqFBh8URbYuvC/Z3C7y','0f0', 1, 'Mama', 2,'Papa', 1);
INSERT INTO  TableUtilisateur (nom, prenom, pseudonyme, motDePasse, couleurTank, idQuestionA, reponseA, idQuestionB, reponseB, visibilite) VALUES ('Cena', 'La', 'B','$2a$12$mvXmdry7Yg5dO5TMUmmNEO9iG7pGXSGQwFLqFBh8URbYuvC/Z3C7y','00f', 1, 'Mama', 2,'Papa', 1);
INSERT INTO  TableUtilisateur (nom, prenom, pseudonyme, motDePasse, couleurTank, idQuestionA, reponseA, idQuestionB, reponseB, visibilite) VALUES ('Bob', 'Po', 'T', '$2a$12$mvXmdry7Yg5dO5TMUmmNEO9iG7pGXSGQwFLqFBh8URbYuvC/Z3C7y','f0f', 1, 'Mama', 2,'Papa', 1);
INSERT INTO  TableUtilisateur (nom, prenom, pseudonyme, motDePasse, couleurTank, idQuestionA, reponseA, idQuestionB, reponseB, visibilite) VALUES ('Bob', 'Po', 'P', 'y','255,255,255', 1, 'Mama', 2,'Papa', 1);

COMMIT;