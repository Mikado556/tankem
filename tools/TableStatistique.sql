--GENERATED
DROP TABLE TableStatistique CASCADE CONSTRAINTS;
DROP TABLE TableTemps CASCADE CONSTRAINTS;
DROP TABLE TableDommage CASCADE CONSTRAINTS;
DROP TABLE TableCoupFeu CASCADE CONSTRAINTS;

CREATE TABLE TableTemps
(
	id_utilisateur NUMBER,
	id_niveau NUMBER,
	tempsCase NUMBER,
	PosX NUMBER,
	PosY NUMBER,
	CONSTRAINT fk_idUtilisateurTableTemps FOREIGN KEY (id_utilisateur) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idNiveauTableTemps FOREIGN KEY (id_niveau) REFERENCES TableNiveau (idNiveau)
);

CREATE TABLE TableDommage
(
	id_user NUMBER,
	id_niveau NUMBER,
	id_arme NUMBER,
	etat VARCHAR2(35) CHECK (etat IN ('Inflige' , 'Recu')),
	Dommage NUMBER,
	PosX NUMBER,
	PosY NUMBER,
	CONSTRAINT fk_idUtilisateurTableDommange FOREIGN KEY (id_user) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idNiveauTableDommange FOREIGN KEY (id_niveau) REFERENCES TableNiveau (idNiveau),
	CONSTRAINT fk_idArmeTableDommange FOREIGN KEY (id_arme) REFERENCES tableTypeArme (idArme)
);

CREATE TABLE TableCoupFeu
(
	id_user NUMBER,
	id_arme NUMBER,
	nbCoupFeu NUMBER,
	CONSTRAINT fk_idUtilisateurTableCoupFeu FOREIGN KEY (id_user) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idNiveauTableCoupFeu FOREIGN KEY (id_arme) REFERENCES tableTypeArme (idArme)
);

CREATE TABLE TableStatistique
(
	id_Stat NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	dateMatch DATE DEFAULT SYSDATE,
	id_user1 NUMBER,
	id_user2 NUMBER,
	id_map NUMBER,
	statut1 VARCHAR(20) CHECK(statut1 IN ('Gagnant', 'Perdant', 'Abandon')),
	statut2 VARCHAR(20) CHECK(statut2 IN ('Gagnant', 'Perdant', 'Abandon')),
	CONSTRAINT fk_idUtilisateur1Statistique FOREIGN KEY (id_user1) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idUtilisateur2Statistique FOREIGN KEY (id_user2) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idNiveauStatistique FOREIGN KEY (id_map) REFERENCES TableNiveau (idNiveau)
);

COMMIT;