#-*- coding: utf-8 -*-
import ttk
from Tkinter import *

import bcrypt
import sys
cheminFichierMain =sys.path[0]
cheminFichierMain = cheminFichierMain + "\\..\\..\\..\\common\\internal"
sys.path.append(cheminFichierMain)

from DTO import *

class MenuAuthentification():
    def __init__(self):
        self.joueur1Connecte = False
        self.joueur2Connecte = False
        
        self.root = Tk()
        w = 1100 # width for the Tk root
        h = 630 # height for the Tk root
        
        self.dto = DTO_Utilisateur()
 
        # get screen width and height
        self.ws = self.root.winfo_screenwidth() # width of the screen
        self.hs = self.root.winfo_screenheight() # height of the screen
        
        # calculate x and y coordinates for the Tk root window
        x = (self.ws/2) - (w/2)
        y = (self.hs/2) - (h/2)

        self.couleurRoot = "royalblue4"
        self.couleurFenetre = "steelblue4"
        self.couleurPolice = "white"
        
        # set the dimensions of the screen 
        # and where it is placed
        self.root.config(bg=self.couleurRoot)
        self.root.geometry('%dx%d+%d+%d' % (w, h, x, y))
        
        self.fenetreAuthentification()
     #   self.buttonvalidation=Button(self.root,text="Validation",font=("Arial",17,"bold"),command=self.creationNiveau)
     #  self.buttonvalidation.grid(row=6,column=6)
        self.root.mainloop()

    def verification(self): 

        self.nom = self.EntryNomUtilisateur1.get()
        password=self.EntryMDPUtilisateur1.get()

        self.nom2 = self.EntryNomUtilisateur2.get()
        password2 = self.EntryMDPUtilisateur2.get()

        choice=0
        choice2=0

  
        if not self.joueur1Connecte:
           choice=1
        elif not self.joueur2Connecte:
           choice=2

        if choice == 1:
            responseHash = bcrypt.hashpw(password,self.dto.getMotDePasse(self.nom)[0])

            if responseHash==self.dto.getMotDePasse(self.nom)[0]:

                self.labelMessageConnexion = Label(self.fenetre, text="suces", bg="green", 
                            fg=self.couleurPolice, font=("Century Gothic", 12),width=30)
                self.labelMessageConnexion.grid(row=3,column=0, columnspan=2, pady=10)

                for item in self.dto.getLesArmesUtilisateur(self.nom):
                    self.listeArmeUtilisateur1.insert(END,item) 

                self.joueur1Connecte = True

                self.stringCouleurTank1 = self.dto.getCouleurUtilisateur(self.nom)[0]
        

            else:
                self.labelMessageConnexion = Label(self.fenetre, text="echec", bg="red", 
                            fg=self.couleurPolice, font=("Century Gothic", 12),width=30)
                self.labelMessageConnexion.grid(row=3,column=0, columnspan=2, pady=10) 

        elif choice == 2:
            responseHash = bcrypt.hashpw(password,self.dto.getMotDePasse(self.nom2)[0])
            print("allo")
            if responseHash == self.dto.getMotDePasse(self.nom2)[0]:
                
                self.labelMessageConnexion = Label(self.fenetre, text="suces", bg="green", 
                            fg=self.couleurPolice, font=("Century Gothic", 12),width=30)
                
                self.labelMessageConnexion.grid(row=3,column=3, columnspan=2, pady=10)
                
                for item in self.dto.getLesArmesUtilisateur(self.nom2):
                    self.listeArmeUtilisateur2.insert(END,item) 

                self.joueur2Connecte = True

                self.stringCouleurTank2 = self.dto.getCouleurUtilisateur(self.nom2)[0]

            else:
                self.labelMessageConnexion = Label(self.fenetre, text="echec", bg="red", 
                            fg=self.couleurPolice, font=("Century Gothic", 12),width=30)
                self.labelMessageConnexion.grid(row=3,column=3, columnspan=2, pady=10) 

       
    def fenetreAuthentification(self):
        self.fenetre = Frame(self.root, bg=self.couleurFenetre)
        self.fenetre.config(padx=50,pady=50)
        self.fenetre.pack()
      #  self.buttonvalidation=Button(self.fenetre,text="Validation",font=("Arial",17,"bold"),command=self.creationNiveau)
      #  self.buttonvalidation.grid(row=6,column=6)

        
        row = 0
        col = 0
 
        
        self.labelTitreFenetre = Label(self.fenetre, text="Authentication", bg= self.couleurFenetre, 
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.labelTitreFenetre.grid(row=row,column=col+1,columnspan=4, pady=10)
        row+=1


        self.labelNomUtilisateur1 = Label(self.fenetre, text="Joueur 1 :", bg= self.couleurFenetre, 
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.labelNomUtilisateur1.grid(row=row,column=col, pady=10)
        col+=1
        
        self.EntryNomUtilisateur1 = Entry(self.fenetre, bg= self.couleurRoot, 
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.EntryNomUtilisateur1.grid(row=row,column=col, pady=10)
        col+=1
     
        self.buttonvalidation=Button(self.fenetre,text="AUTHENTICATION",font=("Arial",17,"bold"),command=self.verification)
        self.buttonvalidation.grid(row=0,column=5)

        self.labelNomUtilisateur2 = Label(self.fenetre, text="Joueur 2 :", bg= self.couleurFenetre, 
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.labelNomUtilisateur2.grid(row=row,column=col+1, pady=10)
        col+=1
        self.EntryNomUtilisateur2 = Entry(self.fenetre, bg= self.couleurRoot, 
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.EntryNomUtilisateur2.grid(row=row,column=col+1, pady=10)
        col=0
        row+=1

        self.labelMDPUtilisateur1 = Label(self.fenetre, text="Mot de passe :", bg= self.couleurFenetre,
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.labelMDPUtilisateur1.grid(row=row,column=col, pady=10)
        col+=1
        
        self.EntryMDPUtilisateur1 = Entry(self.fenetre, bg= self.couleurRoot,show="*",
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.EntryMDPUtilisateur1.grid(row=row,column=col, pady=10)
        col+=1
        
        
        self.labelMDPUtilisateur2 = Label(self.fenetre, text="Mot de passe :", bg= self.couleurFenetre, 
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.labelMDPUtilisateur2.grid(row=row,column=col+1, pady=10)
        col+=1
        self.EntryMDPUtilisateur2 = Entry(self.fenetre, bg= self.couleurRoot,show="*",
                            fg=self.couleurPolice, font=("Century Gothic", 12))
        self.EntryMDPUtilisateur2.grid(row=row,column=col+1, pady=10)

        col=0
        row+=1

        self.labelMessageConnexion = Label(self.fenetre, text="...message...", bg= self.couleurRoot, 
                            fg=self.couleurPolice, font=("Century Gothic", 12),width=30)
        self.labelMessageConnexion.grid(row=3,column=0, columnspan=2, pady=10)

        self.labelMessageConnexion = Label(self.fenetre, text="...message...", bg= self.couleurRoot, 
                            fg=self.couleurPolice, font=("Century Gothic", 12),width=30)
        self.labelMessageConnexion.grid(row=3,column=3, columnspan=2, pady=10)

        row+=1
        col=0

        self.listeArmeUtilisateur1 = Listbox(self.fenetre, width=50,height=20)
        self.listeArmeUtilisateur1.grid(row=row,column=col, columnspan=2, pady=10)
        self.listeArmeUtilisateur1.bind("<<ListboxSelect>>",self.getItemSelected)
        
      #  self.labeldim=Label(self.fenetre,text="FirstGun",font=("Arial",12,"bold"))
     #   self.labeldim.grid(row=5,column=0)
     #   self.dimcarre = Entry(self.fenetre,width=24,bd=11)
     #   self.dimcarre.grid(row=4,column=2,pady=1,padx=10)
      #  self.dimcarresecond = Entry(self.fenetre,width=24,bd=11)
      #  self.dimcarresecond.grid(row=3,column=2,pady=1,padx=10)
        

        self.fenetrerouge = Frame(self.fenetre,width=100,height=200,bg="red")
        self.fenetrerouge.grid(row=4,column=2,pady=10,padx=10)
     #   self.fenetrerouge.config(padx=150,pady=120)
        self.labeldim=Label(self.fenetrerouge,text="FirstGun",font=("Arial",12,"bold"))
        self.labeldim.grid(row=0,column=1)
        self.dimcarresecond = Entry(self.fenetrerouge,width=24,bd=11)
        self.dimcarresecond.grid(row=1,column=1,pady=10,padx=10)
        self.labeldim=Label(self.fenetrerouge,text="SecondGun",font=("Arial",12,"bold"))
        self.labeldim.grid(row=2,column=1)
        self.dimcarrefirst = Entry(self.fenetrerouge,width=24,bd=11)
        self.dimcarrefirst.grid(row=3,column=1,pady=10,padx=10)
       # self.fenetrerouge.grid(row=3,column=2)

        self.fenetrerouge2 = Frame(self.fenetre,width=100,height=200,bg="red")
        self.fenetrerouge2.grid(row=4,column=5)
        self.dimcarresecondB = Entry(self.fenetrerouge2,width=24,bd=11)
        self.dimcarresecondB.grid(row=1,column=1,pady=10,padx=10)
        self.dimcarrefirstB = Entry(self.fenetrerouge2,width=24,bd=11)
        self.dimcarrefirstB.grid(row=3,column=1,pady=10,padx=10)
        self.labeldim=Label(self.fenetrerouge2,text="FirstGun",font=("Arial",12,"bold"))
        self.labeldim.grid(row=0,column=1)
        self.labeldim=Label(self.fenetrerouge2,text="SecondGun",font=("Arial",12,"bold"))
        self.labeldim.grid(row=2,column=1)

        col+=3

        self.listeArmeUtilisateur2 = Listbox(self.fenetre, width=50,height=20)
        self.listeArmeUtilisateur2.grid(row=row,column=col, columnspan=2, pady=25)
        self.listeArmeUtilisateur2.bind("<<ListboxSelect>>",self.getItemSelected2)

        row+=1
        self.buttonJouer=Button(self.fenetre,text="Jouer",font=("Arial",17,"bold"),command=self.cacherFenetre)
        self.buttonJouer.grid(row=row,columnspan=5)

      
    def getItemSelected(self,event):
        indice = self.listeArmeUtilisateur1.curselection()[0] #[0] fait le transtypage en int
        nom = self.listeArmeUtilisateur1.get(indice)

        if self.dimcarrefirst.get() == "":
            self.dimcarrefirst.delete(0,END)
            self.dimcarrefirst.insert(3,nom)
        else:
            self.dimcarresecond.delete(0,END)
            self.dimcarresecond.insert(3,nom)

       

    def getItemSelected2(self,event):
        indice = self.listeArmeUtilisateur2.curselection()[0] #[0] fait le transtypage en int
        nom = self.listeArmeUtilisateur2.get(indice)

        if self.dimcarrefirstB.get() == "":
            self.dimcarrefirstB.delete(0,END)
            self.dimcarrefirstB.insert(3,nom)
        else:
            self.dimcarresecondB.delete(0,END)
            self.dimcarresecondB.insert(3,nom)

    def cacherFenetre(self):
        self.root.destroy()

    def getDonnee(self):
        J1 = [self.nom, self.stringCouleurTank1] 
        J2 = [self.nom2, self.stringCouleurTank2] 
        return J1,J2
