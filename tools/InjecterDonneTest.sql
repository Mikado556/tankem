VARIABLE Nom VARCHAR2(25);
VARIABLE Prenom VARCHAR2(25);
VARIABLE Pseudonyme VARCHAR2(25);
VARIABLE grosseurX NUMBER;
VARIABLE grosseurY NUMBER;
VARIABLE titre VARCHAR2(50);
VARIABLE idUtilISateur NUMBER;
VARIABLE idUtilISateur2 NUMBER;
VARIABLE idMap NUMBER;
VARIABLE statut1 VARCHAR2(25);
VARIABLE statut2 VARCHAR2(25);
VARIABLE nbCoupFeu1 NUMBER;
VARIABLE nbCoupFeu2 NUMBER;
VARIABLE etat1 VARCHAR2(25);
VARIABLE Dommage1 NUMBER;
VARIABLE posX NUMBER;
VARIABLE posY NUMBER;
VARIABLE etat2 VARCHAR2(25);
VARIABLE Doammge2 NUMBER;
VARIABLE tempsCase1 NUMBER;
VARIABLE tempsCase2 NUMBER;

DECLARE
	TYPE array_Prenom IS varray(30) OF VARCHAR2(25);
	arrayPrenom array_Prenom := array_Prenom('Matt','Joanne','Robert','Francis','Flavie','Jannette','Oscar','Paul','Bob','Berta','Alan','David','Keven','Stephane','Joanie','Claudie','Thomas','Guillermo','Koufi','Simba','Guido','Roberto','Alejandro','Billy','Mandy','Julie','Romeo','Carlos','Charle','Marilou');
	TYPE array_Nom IS varray(30) OF VARCHAR2(25);
	arrayNom array_Nom := array_Nom('Tremblay','Dupuis','Auge','Leblanc','Gervais','Zelaya','Diaz','Mao','Renault','Rousseux','Leclair','Baudry','Bedard','Robillard','Sanchez','Potvin','Lebrun','Leroux','Galbert','Smith','Hilter','Tompson','Simpson','West','Tarratino','Torres','Chan','Poutine','Trump','Clinton');
	TYPE array_Pseudonyme IS varray(30) OF VARCHAR2(25);
	arrayPseudonyme array_Pseudonyme := array_Pseudonyme('A0','A1','A2','A3','A4','A5','A6','A7','A8','A9','B0','B1','B2','B3','B4','B5','B6','B7','B8','B9','C0','C1','C2','C3','C4','C5','C6','C7','akilea','psyonik');
	TYPE array_Titre IS varray(30) OF VARCHAR2(25);
	arrayTitre array_Titre := array_Titre('Niveau0','Niveau01','Niveau02','Niveau03','Niveau04','Niveau05','Niveau06','Niveau07','Niveau08','Niveau09','Niveau10','Niveau11','Niveau12','Niveau13','Niveau14','Niveau15','Niveau16','Niveau17','Niveau18','Niveau19');
	TYPE array_TypeBloc IS varray(30) OF VARCHAR2(25);
	arrayTypeBloc array_TypeBloc := array_TypeBloc('mur a.v.i','mur fixe','plancher','mur a.v');
	TYPE array_Statut IS varray(30) OF VARCHAR2(25);
	arrayStatut array_Statut := array_Statut('Gagnant', 'Perdant', 'Abandon');
	TYPE array_Etat IS varray(30) OF VARCHAR2(25);
	arrayEtat array_Etat := array_Etat('Inflige' , 'Recu');
	maxidutil NUMBER;
	idMapMax NUMBER;
	idusernow NUMBER;
	randomArray NUMBER;
	randomDommage NUMBER;
	randomTemps NUMBER;
BEGIN

	FOR i IN 1 .. 30 
	LOOP
		:Prenom := arrayPrenom(i);
		:Nom := arrayNom(i);
		:Pseudonyme := arrayPseudonyme(i);
		SELECT MAX(idUtilisateur) into idusernow FROM TableUtilISateur;
		:idUtilISateur := idusernow + 1;
		INSERT INTO  TableUtilisateur (nom, prenom, pseudonyme, motDePasse, couleurTank, idQuestionA, reponseA, idQuestionB, reponseB, visibilite) VALUES (:Nom, :Prenom, :Pseudonyme,'$2a$12$mvXmdry7Yg5dO5TMUmmNEO9iG7pGXSGQwFLqFBh8URbYuvC/Z3C7y','f00',  1, 'Mama', 2,'Papa', 1);
		INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (:idUtilISateur,1);
	END LOOP;
	FOR i IN 1 .. 20
	LOOP
		:grosseurX := MOD(ABS(dbms_random.random),7) + 6;
		:grosseurY := MOD(ABS(dbms_random.random),7) + 6;
		:titre := arrayTitre(i);
		SELECT MAX(idUtilisateur) into maxidutil FROM TableUtilisateur;
		maxidutil := maxidutil + 1;
		:idUtilISateur := MOD(ABS(dbms_random.random),maxidutil);
		SELECT MAX(idmap) INTO idMapMax FROM tablemapjeu;
		:idMap := idMapMax;
		INSERT INTO TableMapJeu (grosseurX,grosseurY) VALUES (:grosseurX,:grosseurY );
		INSERT INTO TableNiveau(titre,delaiMinimun,delaiMaximun,statut,idUtilisateur,idMap) VALUES(:titre,5,20,'Public',:idUtilISateur ,:idMap);
		FOR y IN 0 .. :grosseurY
		LOOP
			FOR x IN 0 .. :grosseurX
			LOOP
				INSERT INTO TablePositionBloc(idMap,posX,posY,bloc) VALUES (:idMap,x,y,arrayTypeBloc(MOD(ABS(dbms_random.random),4)+1));
			END LOOP;
		END LOOP;
	END LOOP;
	COMMIT;
	FOR i IN 1 .. 50
	LOOP
		SELECT MAX(idUtilisateur) INTO maxidutil FROM TableUtilisateur;
		maxidutil := maxidutil + 1;
		:idUtilISateur := MOD(ABS(dbms_random.random),maxidutil) + 1;
		:idUtilISateur2 := MOD(ABS(dbms_random.random),maxidutil) + 1;
		WHILE :idUtilISateur = :idUtilISateur2 
		LOOP
		   :idUtilISateur2 := MOD(ABS(dbms_random.random),maxidutil);
		END LOOP;
		SELECT MAX(idNiveau) INTO idMapMax FROM tableniveau;
		:idMap := MOD(ABS(dbms_random.random),idMapMax);
		randomArray := MOD(ABS(dbms_random.random),3) + 1;
		IF arrayStatut(randomArray) = 'Gagnant' THEN
		   :statut1 := 'Gagnant';
		   :statut2 := 'Perdant';
		ELSIF arrayStatut(randomArray) = 'Perdant' THEN
		   :statut2 := 'Gagnant';
		   :statut1 := 'Perdant';
		ELSE
		   :statut1 := 'Abandon';
		   :statut2 := 'Abandon';
		END IF;
		SELECT 
			grosseurX, grosseurY INTO :grosseurX, :grosseurY
		FROM 
			TableMapJeu
		WHERE 
			idMap = :idMap;
		FOR y IN 0 .. :grosseurY
		LOOP
			FOR x IN 0 .. :grosseurX
			LOOP
				randomArray := MOD(ABS(dbms_random.random),2) + 1;
				:etat1 := arrayEtat(randomArray);
				randomArray := MOD(ABS(dbms_random.random),2) + 1;
				:etat2 := arrayEtat(randomArray);
				randomDommage := MOD(ABS(dbms_random.random),100);
				:Dommage1 := randomDommage;
				randomDommage := MOD(ABS(dbms_random.random),100);
				:Doammge2 := randomDommage;
				randomTemps := MOD(ABS(dbms_random.random),100);
				:tempsCase1 := randomTemps;
				randomTemps := MOD(ABS(dbms_random.random),100);
				:tempsCase2 := randomTemps;
				INSERT INTO TableDommage (id_user,id_niveau,id_arme,etat,Dommage,PosX,PosY) VALUES (:idUtilISateur,:idMap,1,:etat1,:Dommage1,x,y);
				INSERT INTO TableDommage (id_user,id_niveau,id_arme,etat,Dommage,PosX,PosY) VALUES (:idUtilISateur2,:idMap,1,:etat2,:Doammge2,x,y);
				INSERT INTO TableTemps (id_utilisateur,id_niveau,tempsCase,PosX,PosY) VALUES (:idUtilISateur,:idMap,:tempsCase1,x,y);
				INSERT INTO TableTemps (id_utilisateur,id_niveau,tempsCase,PosX,PosY) VALUES (:idUtilISateur2,:idMap,:tempsCase2,x,y);
			END LOOP;
		END LOOP;
		:nbCoupFeu1 := MOD(ABS(dbms_random.random),100);
		:nbCoupFeu2 := MOD(ABS(dbms_random.random),100);
		INSERT INTO TableStatistique (id_user1,id_user2,id_map,statut1,statut2) VALUES (:idUtilISateur, :idUtilISateur2, :idMap, :statut1,:statut2);
		INSERT INTO TableCoupFeu (id_user,id_arme,nbCoupFeu) VALUES (:idUtilISateur, 1 , :nbCoupFeu1);
		INSERT INTO TableCoupFeu (id_user,id_arme,nbCoupFeu) VALUES (:idUtilISateur2, 1 , :nbCoupFeu2);
	END LOOP;
	COMMIT;
END;
/