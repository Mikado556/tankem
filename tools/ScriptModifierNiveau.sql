-- La TableNiveau contient deja le nom du proprietaire
DROP TABLE tableTypeArme  CASCADE CONSTRAINTS;
DROP TABLE tableAssoArme  CASCADE CONSTRAINTS;
DROP TABLE tableAssoFavoris  CASCADE CONSTRAINTS;
DROP TABLE tableAssoNbFoisJouer  CASCADE CONSTRAINTS;

CREATE TABLE tableTypeArme
(
	idArme NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	nom VARCHAR2(65)
);

CREATE TABLE tableAssoArme
(
	idUtilisateur NUMBER,
	idArme NUMBER,
	CONSTRAINT fk_idUtilisateurArme FOREIGN KEY (idUtilisateur) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idArme FOREIGN KEY (idArme) REFERENCES tableTypeArme (idArme)
);

CREATE TABLE tableAssoFavoris
(
	idUtilisateur NUMBER,
	idNiveau NUMBER,
	CONSTRAINT fk_idUtilisateurFavo FOREIGN KEY (idUtilisateur) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idNiveauFavo FOREIGN KEY (idNiveau) REFERENCES TableNiveau (idNiveau)
);

CREATE TABLE tableAssoNbFoisJouer
(
	idUtilisateur NUMBER,
	idNiveau NUMBER,
	NbFoisJouer NUMBER,
	CONSTRAINT fk_idUtilisateurNbFois FOREIGN KEY (idUtilisateur) REFERENCES TableUtilisateur (idUtilisateur),
	CONSTRAINT fk_idNiveauNbFois FOREIGN KEY (idNiveau) REFERENCES TableNiveau (idNiveau)
);

INSERT INTO tableTypeArme (nom) VALUES ('Canon');
INSERT INTO tableTypeArme (nom) VALUES ('Mitraillette');
INSERT INTO tableTypeArme (nom) VALUES ('Grenade');
INSERT INTO tableTypeArme (nom) VALUES ('Spring');
INSERT INTO tableTypeArme (nom) VALUES ('Shotgun');
INSERT INTO tableTypeArme (nom) VALUES ('Piege');
INSERT INTO tableTypeArme (nom) VALUES ('Missile guide');

INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (1,1);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (1,3);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (1,5);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (2,1);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (2,3);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (2,5);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (3,1);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (3,3);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (3,5);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (4,1);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (4,3);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (4,3);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (5,5);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (5,1);
INSERT INTO tableAssoArme(idUtilisateur, idArme) VALUES (5,3);

INSERT INTO tableAssoFavoris(idUtilisateur, idNiveau) VALUES (1,1);
INSERT INTO tableAssoFavoris(idUtilisateur, idNiveau) VALUES (2,1);
INSERT INTO tableAssoFavoris(idUtilisateur, idNiveau) VALUES (3,1);
INSERT INTO tableAssoFavoris(idUtilisateur, idNiveau) VALUES (4,1);
INSERT INTO tableAssoFavoris(idUtilisateur, idNiveau) VALUES (5,1);






COMMIt;