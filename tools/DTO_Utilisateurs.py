import cx_Oracle
#from SelectionNiveau import *
from DAO_Utilisateurs import *
#voila le Singleton ici x ne pas repeter a chaque fois DTO_NIVEAU et conserver les valeurs
# L'object reste le meme pendant tout la execution

class Singleton(type):
    
     def __init__(cls, name, bases, dct):
         cls.__instance = None
         type.__init__(cls, name, bases, dct)

     def __call__(cls, *args, **kw):
         if cls.__instance is None:
             cls.__instance = type.__call__(cls, *args,**kw)
         return cls.__instance

class DTO_Utilisateurs(object):
    __metaclass__ = Singleton
    def __init__(self):
        
       self.dao=Dao_Utilisateurs() 
       
       self.choixDto = False
       
       self.armeprimaire=""
       self.armesecondaire=""
       self.motdepasse=""
       self.matrix=[]
          
    def set_armeprimaire(self,firstgun):
           self.armeprimaire = firstgun   
        
    def set_armesecondaire(self,secondgun):
           self.map_dimcarrey = secondgun
    
    def set_motdepasse(self,id):
           self.motdepasse = self.dao.recuperer_motdepasse(id)

    def set_matrix(self,id):
           self.matrix = self.dao.recuperer_armes(id)

    def get_matrix(self):
        return self.matrix
                      
    def get_motdepasse(self):
        return self.motdepasse
        
    def get_armeprimaire(self):
        return self.armeprimaire
    
    def get_armesecondaire(self):
        return self.armesecondaire
                  
    def set_choice(self):
        self.choixDto = True

    def get_choice(self):
        return self.choixDto


    


        