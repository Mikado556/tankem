<?php
    require_once("partial/header.php");
    require_once("action/StatistiqueAction.php");
    $stats = new StatistiqueAction();

   
?>

    <link rel="stylesheet" type="text/css" media="all" href="css/stylesProfile.css">

  
  <div id="w">
    <div id="content" class="clearfix">
      <h1><?= $_GET["pseudonyme"]?></h1>    
      <section id="reputation">

      <p>Reputation A <?= $stats->retourner_reputation($_GET["pseudonyme"]) ?></p>
      <p>Reputation B <?= "" ?></p>
      <p>Ratio gagnant perdant<?= $stats->tauxGagnantPerdant()?>%</p>
      <p>Ratio abandon <?= $stats->tauxAbandonTotal()?>%</p>
      <p>Parties gagnees <?= $stats->tauxGagnantUnSeulJoueur($_GET["pseudonyme"])?></p>
      <p>Parties perdues <?= $stats->tauxPerdantUnSeulJoueur($_GET["pseudonyme"])?></p>
      <p>Parties abandonees <?= $stats->tauxAbandonUnSeulJoueur($_GET["pseudonyme"])?></p>
      <p>Parties Total <?= $stats->nbTotalPartie()?></p>
      <p>Total Niveaux crees <?= $stats->nbNiveauCree()?></p>
       <p>L'arme favorite <?=$stats->armePlusUtiliseUnJoueur($_GET["pseudonyme"])?></p>

       <p>Armes:</p>

        <table>
            <th style="border:2px solid white; padding:5px;">Titre</th>
            <th style="border:2px solid white; padding:5px;">Dommage moyen</th>
          <?php #foreach($stats->dommageMoyenParArmeUnSeulJoueur($_GET["pseudonyme"]) as $cle => $value){ ?>
          <tr>
            <td style="border:2px solid white; padding:5px;"><?= "Canon" ?></td>
             <td style="border:2px solid white; padding:5px;"><?= $stats->dommageMoyenParArmeUnSeulJoueur($_GET["pseudonyme"])?></td>
          </tr>
          <?php #}
           ?>
        </table>
    
      <p>Ratio dommage/tirs <?= $stats->dommageMoyenJoueur($_GET["pseudonyme"])?>%</p>
      
       <p>Historique matchs</p>

        <table id="tabla">

            <th style="border:2px solid white; padding:5px;">Id match</th>
            <th style="border:2px solid white; padding:5px;">Resultat</th>
        
          <?php ""#foreach($_SESSION["gamesplayed"] as $clet => $valuex){ ?>

          <tr>
             <td class="boton" style="border:2px solid white;padding:5px;"><a href='resultatpartie.php?variable=$clet'><?= ""#$clet?></td>
             <td class="result" style="border:2px solid white; padding:5px;"><?= ""#$valuex?></td>
          </tr>

          <?php# }
           ?>

        </table>
        <a href="pageInformative.php">Voir heatmap</a>

         
      </section>



    </div><!-- @end #content -->
  </div><!-- @end #w -->

<script type="text/javascript">
$(function(){
  $('#profiletabs ul li a').on('click', function(e){
    e.preventDefault();
    var newcontent = $(this).attr('href');
    
    $('#profiletabs ul li a').removeClass('sel');
    $(this).addClass('sel');
    
    $('#content section').each(function(){
      if(!$(this).hasClass('hidden')) { $(this).addClass('hidden'); }
    });
    
    $(newcontent).removeClass('hidden');
  });
});
</script>

<script type="text/javascript">

$(document).ready(function(){
    $('#tabla td.result').each(function()
    {
        if ($(this).text() == 'perdu')
         {
            $(this).css('background-color','#f00');
         }
         else if ($(this).text() == 'gagne')
         {
            $(this).css('background-color','#49E20E');
         }
          else if ($(this).text() == 'null')
         {
            $(this).css('background-color','#5B4743');
         }
    });
});

</script>

 <script>

document.getElementById("tabla").onclick=function(e){ 
    // obtenemos el elemento sobre el que se ha hecho click
    if(!e)e=window.event; 
    if(!e.target) e.target=e.srcElement; 
    // e.target ahora simboliza la celda en la que hemos hecho click
    // subimos de nivel hasta encontrar un tr
    var TR=e.target;
    while( TR.nodeType==1 && TR.tagName.toUpperCase()!="TR" )
        TR=TR.parentNode;
    var celdas=TR.getElementsByTagName("TD");
    // cogemos la primera celda TD del tr (si existe)
    if( celdas.length!=0 )
        // devolvemos su contenido
        alert( celdas[0].innerHTML );

}

</script>  


<?php
    require_once("partial/footer.php");
?>