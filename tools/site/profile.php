<?php
    require_once("partial/header.php");
    require_once("action/ProfileAction.php");
    $action = new ProfileAction();
    $action->execute();
?>

    <link rel="stylesheet" type="text/css" media="all" href="css/stylesProfile.css">

  
  <div id="w">
    <div id="content" class="clearfix">
      <div id="userphoto"><img src="images/avatar.png" alt="default avatar"></div>
      <h1><?= $_SESSION["pseudonyme"]?></h1>

      <nav id="profiletabs">
        <ul class="clearfix">
          <li><a href="#profile" class="sel">Profile</a></li>
          <li><a href="#tank">Tank</a></li>
          <li><a href="#info">Informations</a></li>
        </ul>
      </nav>
      
      <section id="profile">
        <p>Informations personnelles:</p>
        
        <p class="setting"><span>Nom <img src="images/edit.png" alt="*Edit*"></span><?= $action->getName();?></p>

        <p class="setting"><span>Prénom <img src="images/edit.png" alt="*Edit*"></span><?= $_SESSION["prenom"]?></p>
        
        <p class="setting"><span>Nom de joueur <img src="images/edit.png" alt="*Edit*"></span><?=$_SESSION["pseudonyme"]?></p>
        
    </section>
      
    <section id="tank" class="hidden">
        <p>Personnalisez votre tank:</p>

        <script src="js/jscolor.js"></script>

        <button class="jscolor {valueElement:'chosen-value', onFineChange:'setTankColor(this)'}">
            Couleur
        </button>

        <canvas id="space" width="500" height="300">
            <p>Votre navigateur ne supporte pas les canvas</p>
        </canvas>
        <form method="get" action="profile.php">
          <input type="text" name="couleur" id="idCouleur" readonly="readonly"/>
          <input type="submit" value="Sauvegarder" />
        </form>
        <p id="couleurTank" class="hidden"><?= $_SESSION["couleur"]["0"]?></p>

        <script >
            var valide = true;
            function setTankColor(picker){

                var drawingCanvas = document.getElementById('space');
                ctx = drawingCanvas.getContext('2d');
                ctx.beginPath(); 
                
                if(valide){           
                  ctx.fillStyle = "#"+ document.getElementById("couleurTank").textContent;
                  valide=false;
                }
                else{    
                  ctx.fillStyle = "#" + picker.toString();
                }
                ctx.rect(250, 50, 100, 50);
                ctx.rect(350, 60, 100, 10);
                ctx.rect(200, 100, 200, 100);
                ctx.fill();
                ctx.stroke();

                ctx.beginPath();  
                ctx.arc(200,225,25,0, Math.PI*2, true); 
                ctx.fill();
                ctx.stroke();

                ctx.beginPath();  
                ctx.arc(250,225,25,0, Math.PI*2, true); 
                ctx.fill();
                ctx.stroke();

                ctx.beginPath();  
                ctx.arc(300,225,25,0, Math.PI*2, true); 
                ctx.fill();
                ctx.stroke();

                ctx.beginPath();  
                ctx.arc(350,225,25,0, Math.PI*2, true); 
                ctx.fill();
                ctx.stroke();

                ctx.beginPath();  
                ctx.arc(400,225,25,0, Math.PI*2, true); 
                ctx.fill();

                ctx.stroke();
                //ctx.endPath();
                document.getElementById('idCouleur').value = picker.toString()
               
            }

            setTankColor("000");

        </script>

    </section>
      
      <section id="info" class="hidden">
        <p>Niveaux favoris :</p>
        <table>
            <th style="border:2px solid white; padding:5px;">Titre</th>
          <?php for($i = 0 ; $i < count($_SESSION["favoris"]) ; $i++){ ?>
          <tr>
            <td style="border:2px solid white; padding:5px;"><?= $_SESSION["favoris"][$i]["TITRE"] ?></td>
          </tr>
          <?php } 
          $_SESSION["favoris"] = array(); ?>
        </table>
        <p>Niveaux creé(s) :</p>
        <table>
            <th style="border:2px solid white; padding:5px;">Titre</th>
          <?php for($i = 0 ; $i < count($_SESSION["niveauCree"]) ; $i++){ ?>
          <tr>
            <td style="border:2px solid white; padding:5px;"><?= $_SESSION["niveauCree"][$i]["TITRE"] ?></td>
          </tr>
          <?php }
           $_SESSION["niveauCree"] = array(); ?>
        </table>
        <p>Armurerie :</p>
        <table>
            <th style="border:2px solid white; padding:5px;">Armes</th>
          <?php for($i = 0 ; $i < count($_SESSION["arme"]) ; $i++){ ?>
          <tr>
            <td style="border:2px solid white; padding:5px;"><?= $_SESSION["arme"][$i]["NOM"] ?></td>
          </tr>
          <?php } 
           $_SESSION["arme"] = array();?>
        </table>
      </section>
      

    </div><!-- @end #content -->
  </div><!-- @end #w -->

<script type="text/javascript">
$(function(){
  $('#profiletabs ul li a').on('click', function(e){
    e.preventDefault();
    var newcontent = $(this).attr('href');
    
    $('#profiletabs ul li a').removeClass('sel');
    $(this).addClass('sel');
    
    $('#content section').each(function(){
      if(!$(this).hasClass('hidden')) { $(this).addClass('hidden'); }
    });
    
    $(newcontent).removeClass('hidden');
  });
});
</script>

<?php
    require_once("partial/footer.php");
?>