<?php 
    require_once("partial/header.php");
?>
<section id="contenu" class="contenu">
	<h3>NOM DU NIVEAU : <?= "Rekt get on" ?></h3>
	<div class="cadreMatch">
		<p id="joueurGagnant"><?= "psyonik" ?></p><a href="page-publique.php?pseudonyme=psyonik" title="cliquez ici pour en savoir plus"> <?="la puce mexicaine"?></a>
		<p id="joueurPerdant"><?= "akilea" ?></p><a href="page-publique.php?pseudonyme=akilea" title="cliquez ici pour en savoir plus"> <?="gangster"?></a>
		<p>Match remporté par <?="psyonik" ?></p>
	</div>


<select onchange="getValue(this.value)" style="display:block; margin:1em auto;">
    <option value="1">temps total passé</option>
    <option value="2">endroit où joueur a pris du dommage</option>
    <option value="3">endroit où joueur a donné du dommage</option>
    <option value="4">aucune donnée</option>
</select>

<div id="heatmap1" style="height: 400px; min-width: 310px; max-width: 800px; margin: 0 auto"></div>	
<div id="heatmap2" style="height: 400px; min-width: 310px; max-width: 800px; margin: 0 auto"></div> 

<script type="text/javascript">
    var nom = ''
    function getValue(value){
        console.log(value)
          
        if(value == 1){
            nom = 'temps total passé'
        }else if(value == 2){
            nom = 'endroit où joueur a pris du dommage'

        }else if(value == 3){
            nom = 'endroit où joueur a donné du dommage'
        }else if(value == 4){
            nom = 'aucune donnée'

        }
    }

    //console.log(document.getElementById("joueurGagnant").innerHTML);
    
    $.ajax({
        type: 'GET',                    
        url: 'ajaxHeatmap.php',                
        data: {
            gagnant : document.getElementById("joueurGagnant").innerHTML,
            perdant : document.getElementById("joueurPerdant").innerHTML,
            niveau : "Niveau4"
        }
    })
    .done(function(retour) {
        //console.log(retour)
        retour = JSON.parse(retour);

        var dataGagnant = [];
        for(var i = 0; i < 36; i++){
            var posX = parseInt(retour["gagnant"][i]["POSX"]);
            var posY = parseInt(retour["gagnant"][i]["POSY"]);
            var tempsCase = parseInt(retour["gagnant"][i]["TEMPSCASE"]);
            dataGagnant.push([posX,posY,tempsCase]);     
        }

    	var dataPerdant = [];
        for(var i = 0; i < 36; i++){
            var posXPerdant = parseInt(retour["perdant"][i]["POSX"]);
            var posYPerdant = parseInt(retour["perdant"][i]["POSY"]);
            var tempsCasePerdant = parseInt(retour["perdant"][i]["TEMPSCASE"]);
            dataPerdant.push([posXPerdant,posYPerdant,tempsCasePerdant]);     
        }

        //console.log(dataPerdant, retour["perdant"][25]["POSX"],retour["perdant"][25]["POSY"] );


        $(function () {
        $('#heatmap1').highcharts({
            chart: {
                type: 'heatmap',
                marginTop: 40,
                marginBottom: 80,
                plotBorderWidth: 1
            },

            title: {
                text: 'Heatmap du joueur gagnant'   
            },

            xAxis: {
                categories: ['1','2','3','4','5','6']
            },

            yAxis: {
                categories: ['1', '2', '3', '4', '5','6'],
                title: null
            },

            colorAxis: {
                min: 0,
                minColor: '#FFFFFF',
                maxColor: Highcharts.getOptions().colors[2]
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 280
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sur l\'axe des x <br><b>' +
                        this.point.value + '</b> secondes <br><b>' + this.series.yAxis.categories[this.point.y] + ' sur l\'axe des y</b>';
                }
            },
            series: [{
                name: 'heatmap joueur1',
                borderWidth: 1,
                // [x , y , value]
                data: dataGagnant,
                //data: [[0, 0,retour["gagnant"][0]["TEMPSCASE"]], [0, 1, 66], [0, 2, 11], [0, 3, 5], [0, 4, 9],[0,5,30], [1, 0, 92], [1, 1, 58], [1, 2, 78], [1, 3, 117], [1, 4, 48],[1,5,32], [2, 0, 35], [2, 1, 15], [2, 2, 123], [2, 3, 64], [2, 4, 52],[2,5,74],[3, 0, 72], [3, 1, 132], [3, 2, 114], [3, 3, 19], [3, 4, 16],[3,5,90], [4, 0, 38], [4, 1, 5], [4, 2, 8], [4, 3, 117], [4, 4, 115],[4,5,23], [5, 0, 88], [5, 1, 32], [5, 2, 12], [5, 3, 6], [5, 4, 120], [5,5,92]],
                dataLabels: {
                    enabled: true,
                    color: '#000000'
                }
            }],

        });
    });

    $(function () {
        $('#heatmap2').highcharts({
            chart: {
                type: 'heatmap',
                marginTop: 40,
                marginBottom: 80,
                plotBorderWidth: 1
            },

            title: {
                text: 'Heatmap du joueur perdant'  
            },

            xAxis: {
                categories: ['1', '2', '3', '4', '5', '6']
            },

            yAxis: {
                categories: ['1', '2', '3', '4', '5','6'],
                title: null
            },

            colorAxis: {
                min: 0,
                minColor: '#FFFFFF',
                maxColor: Highcharts.getOptions().colors[5]
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 280
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sur l\'axe des x <br><b>' +
                        this.point.value + '</b> secondes <br><b>' + this.series.yAxis.categories[this.point.y] + ' sur l\'axe des y</b>';
                }
            },
            series: [{
                name: 'heatmap joueur2',
                borderWidth: 1,
                // [x , y , value]
                data : dataPerdant,
                //data: [[0, 0,60], [0, 1, 66], [0, 2, 11], [0, 3, 5], [0, 4, 9],[0,5,30], [1, 0, 92], [1, 1, 58], [1, 2, 78], [1, 3, 117], [1, 4, 48],[1,5,32], [2, 0, 35], [2, 1, 15], [2, 2, 123], [2, 3, 64], [2, 4, 52],[2,5,74],[3, 0, 72], [3, 1, 132], [3, 2, 114], [3, 3, 19], [3, 4, 16],[3,5,90], [4, 0, 38], [4, 1, 5], [4, 2, 8], [4, 3, 117], [4, 4, 115],[4,5,23], [5, 0, 88], [5, 1, 32], [5, 2, 12], [5, 3, 6], [5, 4, 120], [5,5,92]],
                dataLabels: {
                    enabled: true,
                    color: '#000000'
                }
            }],

        });
    });

    });




</script>
<!--<div class="cadreMap">
    <h3>Heatmap</h3>
    <canvas id="canvas" class="dessinerHeatmap"></canvas>
</div>
<script type="text/javascript">

function carte()
{
  var canvas = document.getElementById("canvas"); 
  var context = canvas.getContext("2d");
  context.beginPath();
  var colors = ["yellow","green"];
  context.strokeStyle="blue";
  context.lineWidth="2";  
  var data = [[0,0],[0,1],[0,2],[1,0],[1,1],[1,2],[2,0],[2,1],[2,2],[3,0],[3,1],[3,2],[4,1]]; 
  for (var i = 0; i < data.length; i++) {
    console.log(data[i])
      context.rect(data[i][0]*70,data[i][1]*50,70,50); // on modifie seulement l'axe des x et le width
      context.fillStyle = colors[Math.floor((Math.random() * 2) + 0)];
      //context.fillRect(data[i][0]*50,data[i][1]*50,100,100);
      context.stroke();
      //context.fillStyle = "black";
      //context.font = "16px Arial";
      //context.fillText("A",data[i][0]*50,data[i][1]*50);

    }
}

window.onload=carte;
</script> -->
</section>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/heatmap.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<?php
    require_once("partial/footer.php");
?>