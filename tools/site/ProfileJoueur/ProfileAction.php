<?php 
    require_once("action/CommonAction.php");
    require_once("action/DAO/UserDAO.php");
    require_once("action/utils/Reputation.php");

    class ProfileAction extends commonAction{
        
        private static $PAGE_NAME = "Profile";
        public function __construct (){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
        }

        protected function executeAction(){
            $this->niveau_favori();
            $this->niveau_cree();
            $this->armement();
            $this->couleurTank();
            $this->setReputation();
        }

        private function niveau_favori(){
            $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
            $id_niveau = UserDAO::lireIDFavorisNiveau(intval($id_utilisateur["IDUTILISATEUR"]));
            
            if(!isset($_SESSION["favoris"])){
                $_SESSION["favoris"] = array();
            }

            for($i = 0 ; $i < count($id_niveau) ; $i++){
                $favori_niveau = UserDAO::lireTitreNiveau(intval(array_values($id_niveau[$i])["0"]));
                array_push($_SESSION["favoris"], $favori_niveau);
                
            }        
        }

        private function niveau_cree(){
            $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
            $id_niveau = UserDAO::lireIDTableNiveau(intval($id_utilisateur["IDUTILISATEUR"]));

            if(!isset($_SESSION["niveauCree"])){
                $_SESSION["niveauCree"] = array();
            }
            for($i = 0 ; $i < count($id_niveau); $i++){
                $niveau_cree = UserDAO::lireTitreNiveau(intval(array_values($id_niveau[$i])["0"]));
                array_push($_SESSION["niveauCree"], $niveau_cree);
            }
        }

        private function armement(){
            $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
            $id_arme = UserDAO::lireIDArme(intval($id_utilisateur["IDUTILISATEUR"]));

            if(!isset($_SESSION["arme"])){
                $_SESSION["arme"] = array();
            }
            for($i = 0 ; $i < count($id_arme) ; $i++){
                $arme = UserDAO::lireTypeArme(intval(array_values($id_arme[$i])["0"]));
                array_push($_SESSION["arme"], $arme);
            }
        }

        private function couleurTank(){
            if(isset($_GET["couleur"])){
                UserDAO::updateCouleurTank($_GET["couleur"], $_SESSION["pseudonyme"]);
            }   
        }

        private function setReputation()
        {
          //  $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
          //  $q_pgagnes = UserDAO::combienpartiesgagnes(intval($id_utilisateur["IDUTILISATEUR"]));
          //  $q_pgerdus = UserDAO::combienpartiesperdus(intval($id_utilisateur["IDUTILISATEUR"]));
          //  $q_pabandonees = UserDAO::combienpartiesabandonees(intval($id_utilisateur["IDUTILISATEUR"])); 
         
          //  temps ecoule entre le dernier match et le present
          //  $q_tempsecoule = UserDAO::combienpartiesabandonees(intval($id_utilisateur["IDUTILISATEUR"]));

          //  $id_arme = UserDAO::lireIDArme(intval($id_utilisateur["IDUTILISATEUR"]));
          //  $tirs_arme = UserDAO::liretirsarme(intval($id_utilisateur["IDUTILISATEUR"]));
          // $tniveauxcrees = UserDAO::totalniveaux(intval($id_utilisateur["IDUTILISATEUR"]));
          // $gamesplayed = UserDAO::retournerArraymatchsjoues(intval($id_utilisateur["IDUTILISATEUR"]));

            $q_pgagnes=1;
            $arme_dommage = array(115,100,103,85,130);
            $tirs_arme =array(44,90,80,152,64);
            $id_arme = array(2,6,3,5,2); 

            $id_resultat = array('gagne','null','gagne','perdu','null');
            $id_jeux = array(1012,2022,1011,1010,6666);
             
            $jeux_niveaux = array_combine($id_jeux, $id_resultat);

            $reputation = retourner_reputation($q_pgagnes,$id_arme,$tirs_arme);
            $_SESSION["nomReputation"]=$reputation;

             
            $q_gagnes = 4;
            $q_pgerdus = 2;
            $q_pabandonees = 1;
            $q_tempsecoule = 2;
            $q_niveaux = 4;

            $reputationB = retourner_reputationB($q_gagnes,$q_pgerdus,$q_pabandonees,$q_tempsecoule);
            $_SESSION["nomReputationB"] = $reputationB;

            $ratiogagnesperdus = calculerRatiogagnesperdus($q_gagnes,$q_pgerdus);
            $_SESSION["ratiog_vs_p"] = $ratiogagnesperdus;

            $ratioabandon =(int)calculerRatioabandon($q_gagnes,$q_pgerdus,$q_pabandonees);
            $_SESSION["ratioabandon"] = $ratioabandon;

            $_SESSION["gagnes"] =  $q_gagnes;
            $_SESSION["perdues"] = $q_pgerdus;
            $_SESSION["abandon"] = $q_pabandonees;
            $_SESSION["total"] = $q_gagnes + $q_pgerdus + $q_pabandonees;
            $_SESSION["niveaux"] =  $q_niveaux;

            $_SESSION["armefavorite"] = armelejoueurtirleplus($id_arme,$tirs_arme);

            $arraydommage = array();

            $arraydommage = calculerdommagemoyen( $id_arme,$arme_dommage,$tirs_arme );
            
            $_SESSION["tabdommage"] = $arraydommage;
            $_SESSION["dommagestirs"] = calculerdommagemoyenratio($arme_dommage,$tirs_arme);

            $_SESSION["gamesplayed"]  = $jeux_niveaux;
        }


}