<?php
// =====================================================================================
// Source : http://www.asp-php.net/scripts/asp-php/alphabet-radio-code-morse.php?page=3
// -------------------------------------------------------------------------------------

/**
 * Permet d'encoder une cha�ne et de la retourner en morse
 */

function armeplusutilise($armes,$tirs)

{
    
    $array_armes_tirs = array();

    for($i=0;$i<count($armes);$i++)
   {
      $array_armes_tirs[$i] = array($armes[$i] => $tirs[$i]);
   }

     
    $array_total_tirsarmes = array();

     foreach(  $array_armes_tirs as $pair )
    {
      foreach( $pair as $cle => $value )
    { 
      if(!array_key_exists($cle, $array_total_tirsarmes))
      {
        $array_total_tirsarmes[$cle] = 0;
      }

         $array_total_tirsarmes[$cle]+=$value;
    }
    }

    $armeplusutilise = array_search(max( $array_total_tirsarmes),  $array_total_tirsarmes);  

  
    return $armeplusutilise;
}

function mergearray($arr1,$arr2)
{

      $arraytampon = array();

     for($i=0;$i<count($arr1);$i++)
    {
      $arraytampon[$i] = array($arr1[$i] => $arr2[$i]);
    }

     
    $arraymerged = array();

     foreach(  $arraytampon as $pair )
    {

      foreach( $pair as $cle => $value )
    { 
      if(!array_key_exists($cle, $arraymerged))
      {
        $arraymerged[$cle] = 0;
      }
         $arraymerged[$cle]+=$value;
    }

    }

      return $arraymerged;

}

function retourner_reputation($pgagnes,$armes,$tirs)

{

      $variable_temparme = armeplusutilise($armes,$tirs);

   
  		if ($pgagnes<2)
      {
			   $reputationqualifA = 'generaliste';
  		}

	    if($variable_temparme == 6)
      {
         $reputationqualifA = 'le subtil';
      }
      elseif($variable_temparme == 7)
      {
        $reputationqualifA = 'le hitman';
      }
      elseif($variable_temparme == 2)
      {
         $reputationqualifA = 'le gangster';
      }
       elseif($variable_temparme == 3)
      {
         $reputationqualifA = 'exploseur de tete';
      }
       elseif($variable_temparme == 5)
      {
         $reputationqualifA = 'le dechiqueteur';
      }
       elseif($variable_temparme == 4)
      {
         $reputationqualifA = 'la puce mexicaine';
      }
		
   return $reputationqualifA;
}

function calculertauxabandon($pgagnes,$pperdus,$ppabandonees)
{
    
    $total = $pgagnes + $pperdus + $ppabandonees;
    
    $pourcentage = ($ppabandonees/$total)*100;
    
    return $pourcentage;
}

function calculerexperiencejoueur($pgagnes,$pperdus,$ppabandonees)
{
    $total = $pgagnes + $pperdus + $ppabandonees;
    
    return $total;
}

function calculerRatiogagnesperdus($pgagnes,$pperdus)
{
   $ratiodec = $pgagnes/$pperdus;
   $ratio = $ratiodec*100;
   
   return $ratio;
}

function calculerRatioabandon($pgagnes,$pperdus,$ppabandonees)
{
    $total = $pgagnes + $pperdus + $ppabandonees;
    $ratio = $ppabandonees/$total;
    $ratioabandon = $ratio*100;

    return $ratioabandon;
}


function armelejoueurtirleplus($armes,$tirs)
{
   $variable_temparme = armeplusutilise($armes,$tirs);
   
   if($variable_temparme == 1)
   {
     $armefavorite = 'canon';
   }
   elseif($variable_temparme == 2)
   {
     $armefavorite = 'mitraillete';
   }
   elseif($variable_temparme == 3)
   {
     $armefavorite = 'grenade';
   }
   elseif($variable_temparme == 4)
   {
     $armefavorite = 'spring';
   }
    elseif($variable_temparme == 5)
   {
     $armefavorite = 'shotgun';
   }
    elseif($variable_temparme == 6)
   {
     $armefavorite = 'piege';
   }
    elseif($variable_temparme == 7)
   {
     $armefavorite = 'missile';
   }

   return $armefavorite;
}

function trouverarmedesc($cle)
{
        
    if($cle == 1)
   {
     $arme = 'canon';
   }
   elseif($cle == 2)
   {
     $arme = 'mitraillete';
   }
   elseif($cle == 3)
   {
     $arme = 'grenade';
   }
   elseif($cle == 4)
   {
     $arme  = 'spring';
   }
    elseif($cle == 5)
   {
     $arme  = 'shotgun';
   }
    elseif($cle == 6)
   {
     $arme  = 'piege';
   }
    elseif($cle == 7)
   {
    $arme  = 'missile';
   }

 
 return $arme;     
}

function calculerdommagemoyen($armes,$dommage,$tirs)
{
    
   $array_arme_dommage = mergearray($armes,$dommage);
   $array_arme_tirs = mergearray($armes,$tirs);

   $resultarray = array();

   print_r($array_arme_dommage);
   print_r($array_arme_tirs);

   foreach($array_arme_dommage as $cle => $totaldommage)
   {
        $arme = trouverarmedesc($cle);
        $resultarray[$arme] = (round(($totaldommage/$array_arme_tirs[$cle]),2));
   }

   return $resultarray;
}

function calculerdommagemoyenratio($dommage,$tirs)
{
   
   $sumdommage = array_sum($dommage);
   $sumtirs = array_sum($tirs);

   $ratio =round(($sumdommage/$sumtirs)*100,2);

   return $ratio;
}

function retourner_reputationB($pgagnes,$pperdus,$ppabandonees,$ptempsecoule)
{
    
    $variablereputationB = (int)calculertauxabandon($pgagnes,$pperdus,$ppabandonees);
    $variabletotalmatchs = calculerexperiencejoueur($pgagnes,$pperdus,$ppabandonees);
    $variableratiogagnes = calculerRatiogagnesperdus($pgagnes,$pperdus);

    print_r($variablereputationB);

       if($variablereputationB >= 10)
       {
         $reputationqualifB = 'poltron';
       }
       
       if($ptempsecoule >=7)
       {
         $reputationqualifB = 'fantome';
       }

       if($variabletotalmatchs <= 5)
       {
         $reputationqualifB = 'neophite';
       }

       if( $variableratiogagnes >=50 )
       {
         $reputationqualifB = 'vainqueur';        
       }

       return $reputationqualifB;
}

