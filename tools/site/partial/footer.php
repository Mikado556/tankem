	<footer>
		© 2016 Tankem. Tous droits réservés. Conditions d'utilisation - Politique de confidentialité - 
		Conditions Générales de Vente - Mentions Légales
	</footer>
	<script type="text/javascript" src="js/notify.js"></script>
	<script>

		$(document).ready(function(){
		    $("#submit").click(function(){
				//console.log($("#nomJoueur").val())
				$.ajax({
					type: 'GET', 					
					url: 'ajax.php',				
					data: {
						type : "pseudo",
						nomJoueur : $("#nomJoueur").val()
					}
				})
				.done(function(retour) {
					console.log(retour)
					retour = JSON.parse(retour);

                    $("#nomJoueur").notify(retour,{className: retour,position:"right"});
					if(retour == "success")
					{
						$.notify("Nb de parties débutées dans la dernière heure " + <?="10"?> + "h",
							{autoHideDelay: 10000,arrowShow: false,className:"info",position:"top left"});
						$.notify("Nb total de parties jouées " + <?="100"?>,
							{autoHideDelay: 10000,arrowShow: false,className:"info",position:"top left"});
						$("a").remove('#stats');
						$("#search").append("<a id='stats' href='page-publique.php?pseudonyme="+ $("#nomJoueur").val() + "'>Afficher les statistiques de ce joueur</a>");
						
						//document.getElementById('search').innerHTML += "<a id='stats' href='page-publique.php?pseudonyme="+ $("#nomJoueur").val() + "'>Afficher les statistiques</a>";	
					}
				});	
		    });
		});
		$("#contenu")
		  .css('opacity', 0)
		  .slideDown('slow')
		  .animate(
		    { opacity: 1 },
		    { queue: false, duration: 'slow' } // queue permet à l'animation d'exécuter plusieurs à la fois
		);
	</script>
</body>
</html> 