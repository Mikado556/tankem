<?php 
    require_once("action/LoginAction.php");
    $action = new LoginAction();
    $action->execute();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Tankem</title>
	<meta charset="UTF-8">
    
	<link rel="stylesheet" type="text/css" href="css/decor.css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

</head>
<body>
	<header>
		<h1>Tankem</h1>
        <h3>Connecté en tant que : <?= $action->getName() ?></h3>
	</header>
    <div class="menu">
        <ul>
            <li><a href="index.php">Accueil</a></li>
                <?php if($action->isLoggedIn()) { ?>
                    <li><a href="?disconnect=true">Déconnexion</a></li>
                    <li><a href="profile.php">Profile</a></li>
                <?php }else{?>
                    <li><a href="login.php">Connexion</a></li>
                    <li><a href="inscription.php">Inscription</a></li>
                <?php } ?>
        </ul>
    </div>
