<?php 
    require_once("action/CommonAction.php");
    require_once("action/DAO/UserDAO.php");

    class RechercheAction extends CommonAction{
        public $nom = "";
        private static $PAGE_NAME = "recherche";
        public function __construct (){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
        }

        protected function executeAction(){

            if(!empty($_GET["type"])){
                if($_GET["type"] === "pseudo"){
                    $pseudonyme = UserDAO::lirePseudonyme($_GET["nomJoueur"]);
                    if(isset($pseudonyme)){
                        if($_GET["nomJoueur"] == $pseudonyme["PSEUDONYME"]){
                            $this->nom = "success";
                        }
                    }
                    else{
                        $this->nom = "Ce pseudonyme n'existe pas";
                    }
                }
            }
        }

    }

