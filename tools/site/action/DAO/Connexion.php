<?php 

	class Connexion {
		private static $connexion = null;

		// singleton
		public static function getConnexion() {
			if (empty(Connexion::$connexion)) {
				Connexion::$connexion = new PDO(DB_ALIAS, DB_USER, DB_PASS);
				//permet de lancer les erreurs
				Connexion::$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				//Endroit où les variables sont remplacés ? to var
				Connexion::$connexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}
			return Connexion::$connexion;
		}


		/* Pas nécessaire car quand on ferme la page, la connexion est fermée aussi*/
		public static function closeConnexion() {
			if (!empty(Connexion::$connexion)) {	
				Connexion::$connexion	= null;
			}
		}
	}

