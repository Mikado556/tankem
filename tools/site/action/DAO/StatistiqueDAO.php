<?php 
	require_once("Connexion.php");

	class StatistiqueDAO {
	
		public static function readDommage($pseudonyme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT DOMMAGE, POSX, POSY FROM TableDommage WHERE ID_UTILISATEUR = (SELECT ID FROM TableUtilisateur WHERE PSEUDONYME = ?)");

			$statement->bindParam(1, $pseudonyme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageJoueur = null;
				
			if ($row = $statement->fetchAll()) {
				$dommageJoueur = $row;
			}
				
			return $dommageJoueur;
		}

		public static function readDommageTotal(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT POSX, POSY, SUM(DOMMAGE) FROM TableDommage");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageTotal = null;
				
			if ($row = $statement->fetch()) {
				$dommageTotal = $row;
			}
				
			return $dommageTotal;
		}

		public static function readDommageTotalInflige(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT POSX, POSY, SUM(DOMMAGE) FROM TableDommage WHERE ETAT = 'Inflige'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageTotal = null;
				
			if ($row = $statement->fetch()) {
				$dommageTotal = $row;
			}
				
			return $dommageTotal;
		}

		public static function readDommageTotalRecu(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(DOMMAGE) FROM TableDommage WHERE ETAT = 'reçu'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageTotal = null;
				
			if ($row = $statement->fetch()) {
				$dommageTotal = $row;
			}
				
			return $dommageTotal;
		}

		public static function readDommageTotalInfligeParArme($arme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(DOMMAGE) FROM TableDommage WHERE ETAT = 'Inflige' AND (SELECT ID FROM TableTypeArme WHERE TYPE = ?)");

			$statement->bindParam(1, $arme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageTotalParAme = null;
				
			if ($row = $statement->fetch()) {
				$dommageTotalParAme = $row;
			}
				
			return $dommageTotalParAme;
		}

		public static function readDommageTotalInfligeParArmePourUnJoueur($pseudonyme, $arme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(DOMMAGE) FROM TableDommage WHERE ETAT = 'Inflige' AND ID_USER = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) AND ID_ARME = (SELECT IDARME FROM TableTypeArme WHERE NOM = ?)");
			$statement->bindParam(1, $pseudonyme);
			$statement->bindParam(2, $arme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageTotalParArmePourUnJoueur = null;
				
			if ($row = $statement->fetch()) {
				$dommageTotalParArmePourUnJoueur = $row;
			}
				
			return $dommageTotalParArmePourUnJoueur;
		}

		public static function readDommageTotalInfligePourUnJoueur($pseudonyme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(DOMMAGE) FROM TableDommage WHERE ETAT = 'Inflige' AND ID_USER = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?)");
			$statement->bindParam(1, $pseudonyme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$dommageTotalPourUnJoueur = null;
				
			if ($row = $statement->fetch()) {
				$dommageTotalPourUnJoueur = $row;
			}
				
			return $dommageTotalPourUnJoueur;
		}

		public static function readTempsPasserSurToutesLesCases($pseudonyme, $niveau){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT TEMPSCASE, POSX, POSY FROM TableTemps WHERE ID_UTILISATEUR = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) AND ID_NIVEAU = (SELECT IDNIVEAU FROM TABLENIVEAU WHERE TITRE = ?) ORDER BY POSX, POSY");

			$statement->bindParam(1, $pseudonyme);
			$statement->bindParam(2, $niveau);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$tempsJoueur = null;
				
			if ($row = $statement->fetchAll()) {
				$tempsJoueur = $row;
			}
				
			return $tempsJoueur;		
		}
		public static function readTempsTotalPasserUneSeuleCaseTousLesJoueurs($posX, $posY){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(TEMPSCASE) FROM TableTemps WHERE POSX = ? AND POSY = ?");

			$statement->bindParam(1, $posX);
			$statement->bindParam(2, $posY);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$tempsTotal = null;
				
			if ($row = $statement->fetch()) {
				$tempsTotal = $row;
			}
				
			return $tempsTotal;
		}


		public static function readTempsTotalPasserUneSeuleCaseUnSeulJoueur($pseudonyme, $posX, $posY){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(TEMPSCASE) FROM TableTemps WHERE ID_UTILISATEUR = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) AND POSX = ? AND POSY = ?");

			$statement->bindParam(1, $pseudonyme);
			$statement->bindParam(2, $posX);
			$statement->bindParam(3, $posY);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();		
				
			$tempsTotal = null;
				
			if ($row = $statement->fetch()) {
				$tempsTotal = $row;
			}
				
			return $tempsTotal;
		}

		public static function readCoupFeu($pseudonyme, $arme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT NBCOUPFEU FROM TableCoupFeu WHERE ID_UTILISATEUR = (SELECT ID FROM TableUtilisateur WHERE PSEUDONYME = ?) AND ID_ARME = (SELECT ID FROM TableTypeArme WHERE TYPE = ?)");

			$statement->bindParam(1, $pseudonyme);
			$statement->bindParam(2, $arme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$nbCoupFeu = null;
				
			if ($row = $statement->fetchAll()) {
				$nbCoupFeu = $row;
			}
				
			return $nbCoupFeu;
		}

		public static function readNbCoupFeuTotalUnSeulJoueur($pseudonyme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(NBCOUPFEU) FROM TableCoupFeu WHERE ID_USER = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?)");

			$statement->bindParam(1, $pseudonyme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$coupFeuTotalUnSeulJoueur = null;
				
			if ($row = $statement->fetch()) {
				$coupFeuTotalUnSeulJoueur = $row;
			}
				
			return $coupFeuTotalUnSeulJoueur;
		}
		public static function readNbCoupFeuTotalUneArmeUnSeulJoueur($pseudonyme, $arme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(NBCOUPFEU) FROM TableCoupFeu WHERE ID_USER = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) AND ID_ARME = (SELECT IDARME FROM TableTypeArme WHERE NOM = ?)");

			$statement->bindParam(1, $pseudonyme);
			$statement->bindParam(2, $arme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$coupFeuTotalUnSeulJoueur = null;
				
			if ($row = $statement->fetchAll()) {
				$coupFeuTotalUnSeulJoueur = $row;
			}
				
			return $coupFeuTotalUnSeulJoueur;
		}

		public static function readNbCoupFeuTotalUneArmeTousLesJoueurs($arme){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(NBCOUPFEU) FROM TableCoupFeu WHERE ID_ARME = (SELECT IDARME FROM TableTypeArme WHERE NOM = ?)");

			$statement->bindParam(1, $arme);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$coupFeuTotalTousLesJoueurs = null;
				
			if ($row = $statement->fetch()) {
				$coupFeuTotalTousLesJoueurs = $row;
			}
				
			return $coupFeuTotalTousLesJoueurs;
		}

		public static function readNbCoupFeuTotal(){ #TOUS LES JOUEURS ET TOUTES LES ARMES
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT SUM(NBCOUPFEU) FROM TableCoupFeu");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$coupFeuTotal = null;
				
			if ($row = $statement->fetch()) {
				$coupFeuTotal = $row;
			}
				
			return $coupFeuTotal;
		}

		public static function readStatistiquePartie($pseudonyme1, $pseudonyme2){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT STATUT1, STATUT2, DATE FROM TableStatistique WHERE ID_USER1 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME =?) AND ID_USER2 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?)");

			$statement->bindParam(1, $pseudonyme1);
			$statement->bindParam(2, $pseudonyme2);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$statistiquePartie = null;
				
			if ($row = $statement->fetchAll()) {
				$statistiquePartie = $row;
			}
				
			return $statistiquePartie;
		}

		public static function readNbTotalGagneTousLesJoueursUn(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique WHERE STATUT1 = 'gagnant'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieGagne = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieGagne = $row;
			}
				
			return $totalPartieGagne;
		}

		public static function readNbTotalGagneTousLesJoueursDeux(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT2) FROM TableStatistique WHERE STATUT2 = 'gagnant'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieGagne = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieGagne = $row;
			}
				
			return $totalPartieGagne;
		}

		public static function readTotalPartieGagneJoueurUn($pseudonyme1){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique WHERE STATUT1 = 'gagnant'AND ID_USER1 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) ");

			$statement->bindParam(1, $pseudonyme1);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieGagneJoueurUn = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieGagneJoueurUn = $row;
			}
				
			return $totalPartieGagneJoueurUn;
		}
		public static function readTotalPartieGagneJoueurDeux($pseudonyme2){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT2) FROM TableStatistique WHERE STATUT2 = 'gagnant'AND ID_USER2 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) ");

			$statement->bindParam(1, $pseudonyme2);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieGagneJoueurDeux = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieGagneJoueurDeux = $row;
			}
				
			return $totalPartieGagneJoueurDeux;
		}

		public static function readNbTotalPerduTousLesJoueursUn(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique WHERE STATUT1 = 'perdant'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieGagne = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieGagne = $row;
			}
				
			return $totalPartieGagne;
		}

		public static function readNbTotalPerduTousLesJoueursDeux(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT2) FROM TableStatistique WHERE STATUT2 = 'perdant'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartiePerdu = null;
				
			if ($row = $statement->fetch()) {
				$totalPartiePerdu = $row;
			}
				
			return $totalPartiePerdu;
		}

		public static function readTotalPartiePerdJoueurUn($pseudonyme1){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique WHERE STATUT1 = 'perdant'AND ID_USER1 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) ");

			$statement->bindParam(1, $pseudonyme1);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartiePerdJoueurUn = null;
				
			if ($row = $statement->fetch()) {
				$totalPartiePerdJoueurUn = $row;
			}
				
			return $totalPartiePerdJoueurUn;
		}
		public static function readTotalPartiePerdJoueurDeux($pseudonyme2){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT2) FROM TableStatistique WHERE STATUT2 = 'perdant'AND ID_USER2 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) ");

			$statement->bindParam(1, $pseudonyme2);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartiePerdJoueurDeux = null;
				
			if ($row = $statement->fetch()) {
				$totalPartiePerdJoueurDeux = $row;
			}
				
			return $totalPartiePerdJoueurDeux;
		}

		public static function readNbTotalAbandonTousLesJoueursUn(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique WHERE STATUT1 = 'abandon'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieGagne = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieGagne = $row;
			}
				
			return $totalPartieGagne;
		}

		public static function readNbTotalAbandonTousLesJoueursDeux(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT2) FROM TableStatistique WHERE STATUT2 = 'abandon'");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartiePerdu = null;
				
			if ($row = $statement->fetch()) {
				$totalPartiePerdu = $row;
			}
				
			return $totalPartiePerdu;
		}

		public static function readTotalPartieAbandonJoueurUn($pseudonyme1){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique WHERE STATUT1 = 'abandon'AND ID_USER1 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) ");

			$statement->bindParam(1, $pseudonyme1);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieAbandonJoueurUn = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieAbandonJoueurUn = $row;
			}
				
			return $totalPartieAbandonJoueurUn;
		}
		public static function readTotalPartieAbandonJoueurDeux($pseudonyme2){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT2) FROM TableStatistique WHERE STATUT2 = 'abandon'AND ID_USER2 = (SELECT IDUTILISATEUR FROM TableUtilisateur WHERE PSEUDONYME = ?) ");

			$statement->bindParam(1, $pseudonyme2);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$totalPartieAbandonJoueurDeux = null;
				
			if ($row = $statement->fetch()) {
				$totalPartieAbandonJoueurDeux = $row;
			}
				
			return $totalPartieAbandonJoueurDeux;
		}

		public static function readTotalPartieJoueur(){
			$Connexion = Connexion::getConnexion();
			$statement = $Connexion->prepare("SELECT COUNT(STATUT1) FROM TableStatistique ");


			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$nbPartieTotal = null;
				
			if ($row = $statement->fetch()) {
				$nbPartieTotal = $row;
			}
				
			return $nbPartieTotal;
		}

	}		

