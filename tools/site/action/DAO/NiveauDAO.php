<?php 
	require_once("Connexion.php");

	class NiveauDAO {
		public static function readNbNiveauTotal(){
			$connexion = Connexion::getConnexion();
			$statement = $connexion->prepare("SELECT COUNT(IDNIVEAU) FROM TableNiveau");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute(); 			
				
			$nbNiveauTotal = null;
				
			if ($row = $statement->fetchAll()) {
				$nbNiveauTotal = $row;
			}
				
			return $nbNiveauTotal;
		}
	}