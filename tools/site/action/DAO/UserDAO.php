<?php 
	require_once("Connexion.php");

	class UserDAO {
		
		public static function authenticate($pseudonyme, $mdp) {
			$connexion = Connexion::getConnexion();
			try
			{
				$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$statement = $connexion->prepare("SELECT prenom, nom, pseudonyme, motDePasse, visibilite FROM 
		                                           TableUtilisateur WHERE pseudonyme = ?");
				$statement->bindParam(1, $pseudonyme);  
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute(); 	

				$info = null;
				if($reponse= $statement->fetch()){
					if(password_verify($mdp,array_values($reponse)["3"])){
						$info = $reponse;
					}
				}
				return $info;

			}
			catch(Exception $e)
			{
	  			die($e->getMessage());  			
			}	
			
		}

	    public static function lireIdUtilisateur($nom){
	    	$connexion = Connexion::getConnexion();
	    	$statement = $connexion->prepare("SELECT idUtilisateur FROM TableUtilisateur WHERE pseudonyme = ?");
	    	$statement->bindParam(1, $nom); 
	        $statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$id_utilisateur = null;
					
			if ($row = $statement->fetch()) {
				$id_utilisateur = $row;
			}
						
			return $id_utilisateur;
	    }

        public static function lirePseudonyme($pseudonyme){
            $connexion = Connexion::getConnexion();
            $statement = $connexion->prepare("SELECT PSEUDONYME FROM TableUtilisateur WHERE PSEUDONYME = ?");
            $statement->bindParam(1, $pseudonyme); 
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();

            $pseudo = null;
                    
            if ($row = $statement->fetch()) {
                $pseudo = $row;
            }
                        
            return $pseudo;
        }

        public static function lireIDFavorisNiveau($id_utilisateur){
        	$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("SELECT idNiveau FROM tableAssoFavoris WHERE idUtilisateur=?");
        	$statement->bindParam(1, $id_utilisateur);
        	$statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$id_niveau = null;
					
			if ($row = $statement->fetchAll()) {
				$id_niveau = $row;
			}
			
			return $id_niveau;
        }

        public static function lireIDTableNiveau($id_utilisateur){
        	$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("SELECT idNiveau FROM TableNiveau WHERE idUtilisateur=?");
        	$statement->bindParam(1, $id_utilisateur);
        	$statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$id_niveau = null;
					
			if ($row = $statement->fetchAll()) {
				$id_niveau = $row;
			}
			
			return $id_niveau;
        }

        public static function lireTitreNiveau($id_niveau){
        	$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("SELECT titre FROM TableNiveau WHERE idNiveau=?");
        	$statement->bindParam(1, $id_niveau);
        	$statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$favori_niveau = null;
					
			if ($row = $statement->fetch()) {
				$favori_niveau = $row;
			}
			return $favori_niveau;
        }
        public static function lireIDArme($id_utilisateur){
        	$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("SELECT idArme FROM tableAssoArme WHERE idUtilisateur=?");
        	$statement->bindParam(1, $id_utilisateur);
        	$statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$id_arme = null;
					
			if ($row = $statement->fetchAll()) {
				$id_arme = $row;
			}
			
			return $id_arme;
        }

        public static function readTypeArme(){
    		$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("SELECT nom FROM TableTypeArme");
        	$statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$type_arme = null;
					
			if ($row = $statement->fetchAll()) {
				$type_arme = $row;
			}
			return $type_arme;	
        }

        public static function lireTypeArme($id_arme){
        	$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("SELECT nom FROM TableTypeArme WHERE idArme=?");
        	$statement->bindParam(1, $id_arme);
        	$statement->setFetchMode(PDO::FETCH_ASSOC);
	        $statement->execute();

			$type_arme = null;
					
			if ($row = $statement->fetch()) {
				$type_arme = $row;
			}
			return $type_arme;
        }

        public static function selectCouleurTank($pseudonyme){
    		$connexion = Connexion::getConnexion();
    		$statement = $connexion->prepare("SELECT couleurTank FROM TableUtilisateur WHERE pseudonyme=?");
    		$statement->bindParam(1, $pseudonyme);
    		$statement->execute();

			$couleur = null;
					
			if ($row = $statement->fetch()) {
				$couleur = $row;
			}
			return $couleur;
        }

        public static function updateCouleurTank($couleur, $pseudonyme){
        	$connexion = Connexion::getConnexion();
        	$statement = $connexion->prepare("UPDATE TableUtilisateur SET couleurTank=? WHERE pseudonyme=?");
        	$statement->bindParam(1, $couleur);
        	$statement->bindParam(2, $pseudonyme);
        	$statement->execute();
        }

		public static function selectQuestion(){
			$connexion = Connexion::getConnexion();
			$statement = $connexion->prepare("SELECT Question FROM tableQuestion");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$question = null;
				
			if ($row = $statement->fetchAll()) {
				$question = $row;
			}
				
			return $question;

		}

		public static function inscrireUsager($prenom, $nom, $username, $password, $idQuestionA, $reponseA, $idQuestionB, $reponseB) {
			$connexion = Connexion::getConnexion();
				
			$mdp = password_hash($password, PASSWORD_DEFAULT);
			$statement = $connection->prepare("INSERT INTO TableUtilisateur(nom, prenom, pseudonyme, motDePasse, couleurTank, idQuestionA, reponseA, idQuestionB, reponseB) VALUES (?,?,?,?,?,?,?,?,?)");
			$statement->bindParam(1, $nom);
			$statement->bindParam(2, $prenom);
			$statement->bindParam(3, $username);
			$statement->bindParam(4, $mdp);
			$statement->bindParam(5, $couleurTank);
			$statement->bindParam(6, $idQuestionA);
			$statement->bindParam(7, $reponseA);
			$statement->bindParam(8, $idQuestionB);
			$statement->bindParam(9, $reponseB);
			$statement->execute();			
		}
	}
