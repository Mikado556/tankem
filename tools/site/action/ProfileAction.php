<?php 
    require_once("action/CommonAction.php");
    require_once("action/DAO/UserDAO.php");

    class ProfileAction extends commonAction{
        
        private static $PAGE_NAME = "Profile";
        public function __construct (){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
        }

        protected function executeAction(){
            $this->niveau_favori();
            $this->niveau_cree();
            $this->armement();

            $this->couleurTank();
        }

        private function niveau_favori(){
            $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
            $id_niveau = UserDAO::lireIDFavorisNiveau(intval($id_utilisateur["IDUTILISATEUR"]));
            
            if(!isset($_SESSION["favoris"])){
                $_SESSION["favoris"] = array();
            }

            for($i = 0 ; $i < count($id_niveau) ; $i++){
                $favori_niveau = UserDAO::lireTitreNiveau(intval(array_values($id_niveau[$i])["0"]));
                array_push($_SESSION["favoris"], $favori_niveau);
                
            }        
        }

        private function niveau_cree(){
            $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
            $id_niveau = UserDAO::lireIDTableNiveau(intval($id_utilisateur["IDUTILISATEUR"]));

            if(!isset($_SESSION["niveauCree"])){
                $_SESSION["niveauCree"] = array();
            }
            for($i = 0 ; $i < count($id_niveau); $i++){
                $niveau_cree = UserDAO::lireTitreNiveau(intval(array_values($id_niveau[$i])["0"]));
                array_push($_SESSION["niveauCree"], $niveau_cree);
            }
        }

        private function armement(){
            $id_utilisateur = UserDAO::lireIdUtilisateur($_SESSION["pseudonyme"]);
            $id_arme = UserDAO::lireIDArme(intval($id_utilisateur["IDUTILISATEUR"]));

            if(!isset($_SESSION["arme"])){
                $_SESSION["arme"] = array();
            }
            for($i = 0 ; $i < count($id_arme) ; $i++){
                $arme = UserDAO::lireTypeArme(intval(array_values($id_arme[$i])["0"]));
                array_push($_SESSION["arme"], $arme);
            }
        }

        private function couleurTank(){
            if(isset($_GET["couleur"])){
                UserDAO::updateCouleurTank($_GET["couleur"], $_SESSION["pseudonyme"]);
            }   
        }
}