<?php
	require_once("action/commonAction.php");
	require_once("action/DAO/UserDAO.php");
	require_once("action/VerifierInscriptionAction.php");

	class InscriptionAction extends CommonAction {
		/*private $nom;
		private $prenom;
		private $mdp;
		private $repA;
		private $repB;*/
		public function __construct(){
		}

		protected function executeAction(){
			$nom=!empty($_POST["nom"]) ? $_POST["nom"]:''; 
			$prenom=!empty($_POST["prenom"]) ? $_POST["prenom"]:'';
			$repA=!empty($_POST["repA"]) ? $_POST["repA"]:'';
			$repB=!empty($_POST["repB"]) ? $_POST["repB"]:'';
			if(!empty($_POST["mdp1"]) && !empty($_POST["mdp2"])){
				$mdp1=$_POST["mdp1"];
				$mdp2=$_POST["mdp2"];
				$code = VerifierInscriptionAction::verificationMdp($mdp1,$mdp2);
				if($code == 1){
					UserDAO::inscrireUsager($_POST["prenom"], $_POST["nom"], $_POST["nomJoueur"], $_POST["mdp1"], $_POST["questA"], $_POST["repA"], $_POST["questB"], $_POST["repB"]);
				}
				$_SESSION["code"]= $code;
			}
		}
		public function remplirComboBox(){
			return UserDAO::selectQuestion();
		}
	}







