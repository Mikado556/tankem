<?php 
	session_start();
	require_once("action/db.php");
	abstract class CommonAction{

		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_MODERATOR = 2;
		public static $VISIBILITY_ADMINISTRATOR = 3;


		private $pageVisibility;
		public $pageName;


		public function __construct($pageVisibility,$pageName) {
			$this->pageVisibility = $pageVisibility;
			$this->pageName = $pageName;
		}


		public function getName() {

			$nom = "Invité";

			if (isset($_SESSION["nom"])) {
				$nom = $_SESSION["nom"];
			}

			return $nom;
		}

		public function execute() {
				if (!empty($_GET["disconnect"])) {
					session_unset();
					session_destroy();
					session_start();
					header("location:index.php");
				}

				if (empty($_SESSION["visibility"])) {
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
				}

				if ($_SESSION["visibility"] < $this->pageVisibility){
					header("location:index.php");
					exit;
				}

				$this->executeAction();
			}

		public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}

		protected function setUserCredentials($info){
			if ($info["VISIBILITE"] > CommonAction::$VISIBILITY_PUBLIC) {
				$_SESSION["visibility"] = $info["VISIBILITE"];
				$_SESSION["pseudonyme"] = $info["PSEUDONYME"];
				$_SESSION["prenom"] = $info["PRENOM"];
				$_SESSION["nom"] = $info["NOM"];

				header("location:index.php");
				exit;
			}
		}

		protected abstract function executeAction();

	}
