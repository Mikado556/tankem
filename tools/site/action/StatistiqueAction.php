<?php 
    require_once("action/CommonAction.php");
    require_once("action/DAO/StatistiqueDAO.php");
    require_once("action/DAO/NiveauDAO.php");
    require_once("action/DAO/UserDAO.php");

    class StatistiqueAction extends commonAction{
        
        private static $PAGE_NAME = "stats";
        public function __construct (){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
        }

        protected function executeAction(){

            $this->tauxGagnantPerdant();
            $this->tauxAbandonTotal();
            $this->tauxGagnantTousLesJoueurs();
            $this->tauxPerdantTousLesJoueurs();
            $this->tauxAbandonTousLesJoueurs();
            $this->nbTotalPartie();
            $this->nbNiveauCree();
            $this->armePlusUtiliseUnJoueur($_GET["pseudonyme"]);
            $this->armePlusUtiliseTousLesJoueurs();
            $this->dommageMoyenParArmeUnSeulJoueur($_GET["pseudonyme"]);
            $this->dommageMoyenJoueur($_GET["pseudonyme"]);


        }

        function tauxGagnantPerdant(){
            try{
                if($this->tauxPerdantTousLesJoueurs() == 0){
                    throw new Exception(" 0");
                }
                $taux = $this->tauxGagnantTousLesJoueurs() / $this->tauxPerdantTousLesJoueurs();
                $pourcentage = $taux*100;
                
                return $pourcentage;
            } catch (Exception $e) {
                    echo $e->getMessage();
            }
            
        }

        function tauxAbandonTotal(){
            try{
                
                if($this->nbTotalPartie() == 0){
                    throw new Exception("0");
                }
                $taux = $this->tauxAbandonTousLesJoueurs() / $this->nbTotalPartie();
                $pourcentage = $taux*100;
                return $pourcentage;
            } catch (Exception $e) {
                    echo $e->getMessage();
            }
        }

        function tauxGagnantTousLesJoueurs(){
            $gagnantJoueur1 = StatistiqueDAO::readNbTotalGagneTousLesJoueursUn()['COUNT(STATUT1)'];
            $gagnantJoueur2 = StatistiqueDAO::readNbTotalGagneTousLesJoueursDeux()['COUNT(STATUT2)'];
            $gagnant = $gagnantJoueur1 + $gagnantJoueur2;
            return $gagnant;
        }

        function tauxPerdantTousLesJoueurs(){
            $perdantJoueur1 = StatistiqueDAO::readNbTotalPerduTousLesJoueursUn()['COUNT(STATUT1)'];
            $perdantJoueur2 = StatistiqueDAO::readNbTotalPerduTousLesJoueursDeux()['COUNT(STATUT2)'];
            $perdant = $perdantJoueur1 + $perdantJoueur2;
            return $perdant;
        }

        function tauxAbandonTousLesJoueurs(){
            $abandonJoueur1 = StatistiqueDAO::readNbTotalAbandonTousLesJoueursUn()["COUNT(STATUT1)"];
            $abandonJoueur2 = StatistiqueDAO::readNbTotalAbandonTousLesJoueursDeux()["COUNT(STATUT2)"];
            $abandon = $abandonJoueur1 + $abandonJoueur2;
            return $abandon;
        }

        function tauxGagnantUnSeulJoueur($joueur){
            #on passe le même joueur pour connaître le nombre total de partie gagné par ce joueur
            $gagnantJoueur1 = StatistiqueDAO::readTotalPartieGagneJoueurUn($joueur)['COUNT(STATUT1)'];
            $gagnantJoueur2 = StatistiqueDAO::readTotalPartieGagneJoueurDeux($joueur)['COUNT(STATUT2)'];
            $gagnant = $gagnantJoueur1 + $gagnantJoueur2;
            return $gagnant;
        }

        function tauxPerdantUnSeulJoueur($joueur){
            #on passe le même joueur pour connaître le nombre total de partie perdu par ce joueur
            $perdantJoueur1 = StatistiqueDAO::readTotalPartiePerdJoueurUn($joueur)['COUNT(STATUT1)'];
            $perdantJoueur2 = StatistiqueDAO::readTotalPartiePerdJoueurDeux($joueur)['COUNT(STATUT2)'];
            $perdant = $perdantJoueur1 + $perdantJoueur2;
            return $perdant;
        }

        function tauxAbandonUnSeulJoueur($joueur){
            #on passe le même joueur pour connaître le nombre total de partie abandonné par ce joueur
            $abandonJoueur1 = StatistiqueDAO::readTotalPartieAbandonJoueurUn($joueur)['COUNT(STATUT1)'];
            $abandonJoueur2 = StatistiqueDAO::readTotalPartieAbandonJoueurDeux($joueur)['COUNT(STATUT2)'];
            $abandon = $abandonJoueur1 + $abandonJoueur2;
            return $abandon;
        }

        function nbTotalPartie(){
            #var_dump(StatistiqueDAO::readTotalPartieJoueur());exit;
            $nbTotalPartie = StatistiqueDAO::readTotalPartieJoueur()['COUNT(STATUT1)'];
            return $nbTotalPartie;
        }

        function nbNiveauCree(){
            #var_dump(NiveauDAO::readNbNiveauTotal());exit;
            $nbNiveau = NiveauDAO::readNbNiveauTotal()[0]['COUNT(IDNIVEAU)'];
            return $nbNiveau;
        }

        function armePlusUtiliseUnJoueur($pseudonyme){

            $armes = UserDAO::readTypeArme();
            $array_armes_tirs = array();
            for($i=0;$i<count($armes);$i++){
                $array_armes_tirs[$armes[$i]["NOM"]] = StatistiqueDAO::readNbCoupFeuTotalUneArmeUnSeulJoueur($pseudonyme, $armes[$i]["NOM"]);
            }
            $armeplusutilise = array_search(max($array_armes_tirs),  $array_armes_tirs);
            return $armeplusutilise;
        }

        function armePlusUtiliseTousLesJoueurs(){

            $armes = UserDAO::readTypeArme();
            $array_armes_tirs = array();
            for($i=0;$i<count($armes);$i++){
                $array_armes_tirs[$armes[$i]["NOM"]] = StatistiqueDAO::readNbCoupFeuTotalUneArmeTousLesJoueurs($armes[$i]["NOM"]);

                var_dump( $armes[$i]["NOM"], $array_armes_tirs[$armes[$i]["NOM"]]["SUM(NBCOUPFEU)"]);
            }
            $armeplusutilise = array_search(max($array_armes_tirs),  $array_armes_tirs);
            var_dump($armeplusutilise);

            return $armeplusutilise;
        }

        function dommageMoyenParArmeUnSeulJoueur($pseudonyme){

                $armes = UserDAO::readTypeArme();
                $array_armes_tirs = array();
                $array_armes_dommage = array();
                $tauxParArme = array();
                for($i=0;$i<count($armes);$i++){
                    $array_armes_tirs[$armes[$i]["NOM"]] = StatistiqueDAO::readNbCoupFeuTotalUneArmeUnSeulJoueur($pseudonyme, $armes[$i]["NOM"]);
                    $array_armes_dommage[$armes[$i]["NOM"]] = StatistiqueDAO::readDommageTotalInfligeParArmePourUnJoueur($pseudonyme, $armes[$i]["NOM"]);
                    /*
                    if($array_armes_tirs[$armes[$i]["NOM"]]["SUM(NBCOUPFEU)"] == null){
                        return array();
                    }*/
                    $tauxParArme[$armes[$i]["NOM"]] = ($array_armes_dommage[$armes[$i]["NOM"]]["SUM(DOMMAGE)"] / $array_armes_tirs[$armes[$i]["NOM"]][0]["SUM(NBCOUPFEU)"])*100;
                    return $tauxParArme["Canon"]; #well...
                    #var_dump($tauxParArme, $array_armes_dommage[$armes[$i]["NOM"]]["SUM(DOMMAGE)"], $array_armes_tirs[$armes[$i]["NOM"]][0]["SUM(NBCOUPFEU)"]);exit;
                    #var_dump($tauxParArme[$armes[0]["NOM"]]);exit;
                    
                }
                return $tauxParArme["Canon"];
            
        }

        function dommageMoyenJoueur($pseudonyme){
            try{
                $dommageTotal = StatistiqueDAO::readDommageTotalInfligePourUnJoueur($pseudonyme);
                $nbCoupFeu = StatistiqueDAO::readNbCoupFeuTotalUnSeulJoueur($pseudonyme);
                if($nbCoupFeu['SUM(NBCOUPFEU)'] == 0){
                    throw new Exception("0");
                }
                #var_dump($dommageTotal['SUM(DOMMAGE)'],$nbCoupFeu['SUM(NBCOUPFEU)']);exit;
                $dommageMoyen = $dommageTotal['SUM(DOMMAGE)']/$nbCoupFeu['SUM(NBCOUPFEU)'];
                return $dommageMoyen;

            } catch (Exception $e) {
                    echo $e->getMessage();
            } 
        }


        function retourner_reputation($joueur){

            $armeplusutilise = $this->armePlusUtiliseUnJoueur($joueur);
            
            #var_dump($tirs[$armes[0]["NOM"]][0]["SUM(NBCOUPFEU)"]);exit;
            #$variable_temparme = armeplusutilise($armes,$tirs);
            if ($this->tauxGagnantUnSeulJoueur($joueur)<2){
                $reputationqualifA = 'generaliste';
            }

            if($armeplusutilise == 'Piege')
            {
                $reputationqualifA = 'le subtil';
            }
            elseif($armeplusutilise == 'Missile guide')
            {
                $reputationqualifA = 'le hitman';
            }
            elseif($armeplusutilise == 'Mitrailette')
            {
                $reputationqualifA = 'le gangster';
            }
            elseif($armeplusutilise == 'Grenade')
            {
                $reputationqualifA = 'exploseur de tete';
            }
            elseif($armeplusutilise == 'Shotgun')
            {
                $reputationqualifA = 'le dechiqueteur';
            }
            elseif($armeplusutilise == 'Spring')
            {
                $reputationqualifA = 'la puce mexicaine';
            }

            return $reputationqualifA;
        }

        function retourner_reputationB($ptempsecoule)
        {
            
            $variablereputationB = (int)calculertauxabandon($pgagnes,$pperdus,$ppabandonees);
            $variabletotalmatchs = $this->nbTotalPartie();
            $variableratiogagnes = $this->tauxGagnantPerdant();

            print_r($variablereputationB);

               if($variablereputationB >= 10)
               {
                 $reputationqualifB = 'poltron';
               }
               
               if($ptempsecoule >=7)
               {
                 $reputationqualifB = 'fantome';
               }

               if($variabletotalmatchs <= 5)
               {
                 $reputationqualifB = 'neophite';
               }

               if( $variableratiogagnes >=50 )
               {
                 $reputationqualifB = 'vainqueur';        
               }

               return $reputationqualifB;
        }



}
