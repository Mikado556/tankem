<?php 
    require_once("action/CommonAction.php");
    require_once("action/DAO/UserDAO.php");

    class LoginAction extends commonAction{
        
        public $mauvaisLogin = false;
        private static $PAGE_NAME = "Connexion";
        public function __construct (){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
        }

        protected function executeAction(){
            if(!empty($_POST["login"])){
                $info = UserDAO::authenticate($_POST["login"],$_POST["pwd"]);
                if (isset($info))
                {
                    $_SESSION["couleur"] = UserDAO::selectCouleurTank($_POST["login"]);
                    parent::setUserCredentials($info);
                }
                else
                {
                    $this->mauvaisLogin = true;
                }
            }
        }

    }
