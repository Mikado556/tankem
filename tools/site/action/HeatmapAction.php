<?php 
    require_once("action/CommonAction.php");
    require_once("action/DAO/StatistiqueDAO.php");

    class HeatmapAction extends CommonAction{
        public $temps = array();

        private static $PAGE_NAME = "heatmap";
        public function __construct (){
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC,self::$PAGE_NAME);
        }

        protected function executeAction(){

            $tempsGagnant = StatistiqueDAO::readTempsPasserSurToutesLesCases($_GET["gagnant"], $_GET["niveau"]);
            $tempsPerdant = StatistiqueDAO::readTempsPasserSurToutesLesCases($_GET["perdant"], $_GET["niveau"]);
            if(isset($tempsGagnant) and isset($tempsPerdant)){
                $this->temps["gagnant"] = $tempsGagnant;
                $this->temps["perdant"] = $tempsPerdant;
               # var_dump($this->temps["perdant"]);exit;
            }
            else{
                $this->temps["gagnant"] = "failed";
                $this->temps["perdant"] = "failed";
            }
            
            
        }

    }

