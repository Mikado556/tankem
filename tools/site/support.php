<?php
require_once("partial/header.php");
?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="stylesheet" href="template-contact/css/style.css">
  <form action="index.html" class="contact">
    <fieldset class="contact-inner">
      <p class="contact-input">
        <input type="text" name="name" placeholder="Nom…" autofocus>
      </p>

      <p class="contact-input">
        <label for="select" class="select">
          <select name="subject" id="select">
            <option value="" selected>Choisir un sujet…</option>
            <option value="1">Signaler un bug</option>
            <option value="1">Signaler un joueur</option>
            <option value="1">Autre</option>
          </select>
        </label>
      </p>

      <p class="contact-input">
        <textarea name="message" placeholder="Message…"></textarea>
      </p>

      <p class="contact-submit">
        <input type="submit" value="Envoyer Message">
      </p>
    </fieldset>
  </form>
<?php
require_once("partial/footer.php");
?>