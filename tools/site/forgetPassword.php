<?php
	require_once("partial/header.php");
    require_once("action/ForgetPasswordAction.php");
    $fpa = new ForgetPasswordAction();
    $fpa->execute();
    $next=$_SESSION["noValid"];
    #var_dump($next);
    $message="";
    $code=!empty($_SESSION["code"]) ? $_SESSION["code"] : 0;
    #$code=3;
    switch ($code) {
        case 0:
            $message="";
            break;
        case 1:
            $message="Le mot de passe trop court";
            break;
        case 2:
            $message="Les 2 mots de passe ne correspondent pas";
            break;
        case 3:
            $message="Le mot de passe doit contenir au moins 1 lettre minuscule";
            break;
        case 4:
            $message="Le mot de passe doit contenir au moins 1 lettre majuscule";
            break;
        case 5:
            $message="Le mot de passe doit contenir au moins 1 chiffre";
            break;
        case 6:
            $message="Le mot de passe doit contenir au moins 1 des symboles suivants:
            !@#$%?&*()";
            break;
        default:
            # code...
            break;
        }
?>
	<div class="cadreForgetMdp">
		<form method="post">
			<div class="containerForgetMdp">
				<p>Oublie de mot de passe</p>
                <p><?=$message?></p>
                <?php if($next==0){ ?>
				<p>Nom du joueur</p>
				<input type="text" name="nom">
                <?php }elseif($next==1){ ?>
				    <p>Question secrète A : </p>
				    <input type="text" name="repA">
				    <p>Question secrète B : </p>
				    <input type="text" name="repB">
                <?php }elseif($next==2){ ?>
				    <p>Nouveau mot de passe</p> 
				    <input type="password" name="mdp1">
				    <p>Répéter mot de passe</p>
				    <input type="password" name="mdp2">  
                <?php }?>
                <input id="idSuivant" type="submit" value="suivant">
            </div>
        </form>
    </div>
<?php 
    require_once("partial/footer.php");
    if($next==2){ ?>
        <script type="text/javascript">document.getElementById('idSuivant').value="changer";</script>
<?php } ?>