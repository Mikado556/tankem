<?php 
    require_once("partial/header.php");
 ?>
<div id="search" class="search">
	<input id="nomJoueur" type="search" placeholder="Trouver un joueur..." />
	<input id="submit" type="submit" value="rechercher" />
</div>

<div class="intro">
	<div class="introLegende">
		<h1>Tankem : Jeu multijoueurs [2016] </h1>
		<p> Jeu de guerre qui offre un pvp dynamique. Faites du 1vs1. Gagnez des multitudes d'armes en battant vos adversaires.
			Personnalisez votre map de 6 par 6 à 12 par 12. Rendez votre map publique, privée ou inactif. 
			Ajoutez des maps à vos favoris. Changez la couleur de votre tank.
			Et bien plus encore... Vous voulez rejoindre notre communauté ? <a href="inscription.php">cliquez-ici !</a>
		</p>
	</div>
</div>

 <?php
    require_once("partial/footer.php");
?>