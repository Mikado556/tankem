<?php
	require_once("partial/header.php");
	require_once("action/InscriptionAction.php");
	$actionInscription = new InscriptionAction();
	$questions = $actionInscription->remplirComboBox();
    $actionInscription->execute();
    $message="";
    $code=!empty($_SESSION["code"]) ? $_SESSION["code"] : 99;
	switch ($code) {
		case 0:
			$message="INSCRIPTION REUSSI";
			break;
		case 1:
			$message="Le mot de passe trop court";
			break;
		case 2:
			$message="Les 2 mots de passe ne correspondent pas";
			break;
		case 3:
			$message="Le mot de passe doit contenir au moins 1 lettre minuscule";
			break;
		case 4:
			$message="Le mot de passe doit contenir au moins 1 lettre majuscule";
			break;
		case 5:
			$message="Le mot de passe doit contenir au moins 1 chiffre";
			break;
		case 6:
			$message="Le mot de passe doit contenir au moins 1 des symboles suivants:
			!@#$%?&*()";
			break;
		case 99:
			$message="Cette box affichera les critères manquants à votre mot de passe";
			break;
		
		default:
			# code...
			break;
	}
?>
	<div id="test" class="cadreNotif">
		<h2>Notifications</h2>
		<p style="text-align:center;"><?=$message?></p>
		<script>document.getElementById('test').style.display="block";</script>
		<button onclick="document.getElementById('test').style.display ='none';">ok</button>
	</div>
	<!-- CADRE D'INSCRIPTION -->
	<div id="idCadreInscription" class="cadreInscription">
		<form method="post">
			<h3>INSCRIVEZ-VOUS</h3>
			<div>
				<div class="divInscriptionTxt">
					<p>Nom</p>
					<p>Prénom</p>
                    <p>Nom du joueur</p>
					<p>Mot de passe</p>
					<p>Tapez à nouveau le mot de passe</p>
					<p>Question secrète A</p>
					<p>Réponse secrète A</p>
					<p>Question secrète B</p>
					<p>Réponse secrète B</p>
				</div>
				<div class="divInscriptionInput">
					<input id="idnom" type="text" name="nom" placeholder="Nom">
					<input id="idprenom" type="text" name="prenom" placeholder="Prénom">
                    <input id="idNomJoueur" type="text" name="nomJoueur" placeholder="Nom du joueur">
					<input id="idmdp1" type="password" name="mdp1" placeholder="Mot de passe">
					<input id="idmdp2" type="password" name="mdp2" placeholder="confirmez à nouveau">
					<select name="questA">
					<?php
						for($i=0; $i < count($questions); $i++)
						{ 
							var_dump($questions[$i]);
							?>
							<option value=<?=array_values($questions[$i])["0"]?>><?= array_values($questions[$i])["0"]?></option>
							<?php
						}

					?>
					</select>
					<input id="idrepA" type="text" name="repA" placeholder="Votre réponse A ...">
					<select name="questB">
					<?php
						for($i=0; $i < count($questions); $i++)
						{ 
							var_dump($questions[$i]);
							?>
							<option value=<?=array_values($questions[$i])["0"]?>><?= array_values($questions[$i])["0"]?></option>
							<?php
						}

					?>
					</select>
					<input id="idrepB" type="text" name="repB" placeholder="Votre réponse B ...">
				</div>			
				<input id="idInscrire" type="submit" name="inscrire" value="Valider">
				<a href="login.php">SIGN UP</a>
			</div>
		</form>
	</div>
<?php
	require_once("partial/footer.php");
?>